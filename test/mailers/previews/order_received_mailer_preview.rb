# Preview all emails at http://localhost:3000/rails/mailers/order_received_mailer
class OrderReceivedMailerPreview < ActionMailer::Preview
	def sample_mail_preview
		OrderReceivedMailer.sample_email(User.all[1], Order.last)
	end
end
