Rails.application.routes.draw do
  get 'notifications/index'

  # devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  namespace :api do
    namespace :v1 do
      get 'combos' => 'combos#index', :as => 'combos'
      get 'products' => 'products#index', :as => 'products' 
      get 'categories' => 'categories#index', :as => 'categories'
      get 'brands' => 'brands#index', :as => 'brands' 
      get 'orders' => 'orders#index', :as => 'orders'
      post 'orders' => 'orders#create'
      post 'users' => 'users#create'
      post 'sessions' => 'sessions#create'
      delete 'sessions' => 'sessions#destroy'
      get 'active_schedules' => 'active_schedule#index', :as => 'active_schedules'
      post 'device_tokens' => 'device_tokens#create'
    end
    namespace :v2 do
      get 'closest_company' => 'companies#closest_company', :as => 'closest_company'
      get 'valid_coupon' => 'coupons#valid_coupon', :as => 'valid_coupon'
      get 'categories' => 'categories#index', :as => 'categories'
      get 'brand_products' => 'brand_products#index', :as => 'brand_products'
      get 'brands' => 'brands#index', :as => 'brands' 
      post 'sessions', to: 'sessions#create'
      delete 'sessions', to: 'sessions#destroy'
      post 'registrations', to: 'registrations#create'
      get 'active_schedules' => 'active_schedule#index', :as => 'active_schedules'
      post 'device_tokens' => 'device_tokens#create'
      post 'payments/register_card' => 'payments#register_card'
      # post 'payments/register_token' => 'payments#register_token'
      post 'net_promoting_scores' => 'net_promoting_scores#create'
      get 'orders' => 'orders#index'
      patch 'orders/:id' => 'orders#change_state'
      resources :companies do
        resources :products, only: [:index]
        resources :combos, only: [:index]
        get 'find_items' => 'companies#find_items', :as => 'find_items'
        post 'orders' => 'orders#create'
        get 'company_banners' => 'companies#company_banners', :as => 'company_banners'
      end
      get 'fidelity_points' => 'me#fidelity_points', :as => 'fidelity_points'
      get 'my_orders' => 'me#my_orders', :as => 'my_orders'
      get 'credit_cards' => 'me#credit_cards', :as => 'credit_cards'
      delete 'credit_cards', to: 'payments#delete_card'
    end
  end
end
