class BaWorker
  # initialize and naive workers pool
 
  def initialize( params = {})
    @pool = Queue.new
    @workers_count = 20
    @workers = []
  end
 
 
  # Queue and object and wait for the workers to process it
 
  def add_posts_to_pool(object, user)
    @pool << Proc.new do
      # Do something with your object
      next_page = object.next_page
      # Be aware of a return or stop condition here, Open Graph not always is clear with Next Page
      unless next_page.nil?
        user.retrieved_likes += next_page.map { |l| {:user_name => user.full_name, :user_id => user.id, :like_name => l["name"]} }
        add_posts_to_pool(next_page, user)
      end
    end
  end
 
  # Execute Workers to process the Queue
  #
  # Returns: void
  #
 
  def run_workers
    @workers_count.times do |n|
      @workers << Thread.new(n+1) do |my_n|
        while (task = @pool.pop(true) rescue nil) do
          task.call if task
          p "#{@pool.size} jobs pending"
        end
      end
    end
    @workers.map(&:join) rescue nil
  end
 
end