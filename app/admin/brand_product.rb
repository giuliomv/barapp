ActiveAdmin.register BrandProduct do
	permit_params :name, :brand_id, :img_v2_url, :description, :is_fidelity, :fidelity_points
	includes :brand

	filter :brand, :if => proc{current_admin_user.superadmin}, label: 'Marca'
 	filter :name, :if => proc{current_admin_user.superadmin}, label: 'Nombre'
 	filter :is_fidelity, :if => proc{current_admin_user.superadmin}, label: 'Fidelización'

	index do
		column("Nombre", :name)
	    column("Descripción", :description)
	    column("Marca", :brand)
	    column("Fidelización", :is_fidelity)
	    column("Puntos de Fid.", :fidelity_points)
	    actions
	end

	show do
		attributes_table do
	      row :name
	      row :description
	      row :brand
	      row :img_v2_url do
	      	image_tag(brand_product.img_v2_url.url(:thumb), :height => '100')
	      end
	    end
	end

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs 'Detalles' do 
			input :brand, label: 'Marca', :as => :select, :collection => Brand.all.map{|u| ["#{u.name}", u.id]}  unless current_admin_user.company_admin
			input :name, label: 'Nombre' unless current_admin_user.company_admin
			input :description, label: 'Descripción' unless current_admin_user.company_admin
			input :is_fidelity, label: 'Fidelización' unless current_admin_user.company_admin
			input :fidelity_points, label: 'Puntos de fidelización' unless current_admin_user.company_admin
			input :img_v2_url, label: 'Imagen de producto', :as => :file unless current_admin_user.company_admin
		end
		f.actions
	end


end
