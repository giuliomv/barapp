ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    if current_admin_user.company_admin
        columns do
            column do
                panel "Manejo de Productos" do
                    ul do
                        li link_to 'Productos', admin_company_products_path(current_admin_user.company)
                        li link_to 'Combos', admin_company_combos_path(current_admin_user.company)
                    end
                end
            end
            column do
                panel "Manejo de Horarios" do
                    ul do
                        li link_to 'Textos para horario', admin_company_schedule_texts_path(current_admin_user.company)
                        li link_to 'Horarios activos', admin_company_active_schedules_path(current_admin_user.company)
                    end
                end
            end
            column do
                panel "Manejo de ventas" do
                    ul do
                        li link_to 'Órdenes', admin_orders_path
                    end
                end
            end
        end
    else
        columns do
            column do
                panel "Manejo de Clientes" do
                    ul do
                        li link_to 'Usuarios', admin_admin_users_path
                        li link_to 'Compañías', admin_companies_path
                    end
                end
            end
            column do
                panel "Manejo de Stock" do
                    ul do
                        li link_to 'Categorías', admin_categories_path
                        li link_to 'Marcas', admin_brands_path
                    end
                end
            end
            column do
                panel "Manejo de Configuración" do
                    ul do
                        li link_to 'Estado de las órdenes', admin_order_states_path
                    end
                end
            end
        end
    end
  end
end
