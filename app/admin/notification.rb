ActiveAdmin.register_page "Ordenes Nuevas" do
	content do
		if current_admin_user.company_admin
			render :partial => '/new_orders/index.html.erb', :locals => {:orders => Order.where(:company_id => current_admin_user.company_id).order(created_at: :desc).includes(:company).limit(10) }
		elsif current_admin_user.superadmin
			render :partial => '/new_orders/index.html.erb', :locals => {:orders => Order.all.order(created_at: :desc).includes(:company).limit(10) }
		end
	end
end