ActiveAdmin.register PushNotification do

	permit_params :notification_title, :notification_message, :to_android, :to_ios, target_users: []

	controller do
	  def create
	  	p "#{permitted_params[:push_notification]}"
	  	target_users = permitted_params[:push_notification][:target_users].reject!(&:blank?)
	  	push_obj = permitted_params[:push_notification].except(:target_users)
	  	push = PushNotification.new(push_obj)
	  	
		if push.save!
			if target_users.empty?
		     	push.send_notification_to_all
		    else 
				push.send_notifications_to_users(target_users)
		    end
			redirect_to admin_push_notifications_path
		end
	  end
	end


	form do |f|
		f.inputs "Datos de la notificación" do
		  f.input :notification_title, label: 'Título'
		  f.input :notification_message, label: 'Contenido'
		  f.input :to_android, label: 'Para android'
		  f.input :to_ios, label: 'Para iOS'
		  f.input :target_users, :as => :select, :collection => User.notifiable.map{|u| ["#{u.full_name}", u.id]},:input_html => { :multiple => true, :class => "chosen" } 
		end
		f.actions
	end

end
