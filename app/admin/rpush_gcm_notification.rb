ActiveAdmin.register Rpush::Gcm::Notification do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
	index do
		selectable_column
		id_column
		column :data
		column :delivered
		column :delivered_at
		column :failed
		column :failed_at
		column :error_code
		column :error_description
		column :created_at
		column :registration_ids
		column :app
	end

end
