ActiveAdmin.register_page "Mapa de órdenes" do
	content do
		if current_admin_user.superadmin
			render :partial => '/gmaps4rails/mapOrderLocations.html.erb', :locals => {:orders => Order.gmaps_hash(params[:q]), :params => params[:q] }
		end
	end

	page_action :scope_markers, method: :get do
		redirect_to admin_mapa_de_ordenes_path(params)
	end
end