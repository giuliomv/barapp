ActiveAdmin.register Combo do
	permit_params :name, :description, :price, :img_url, :img_v2_url, :img_shopping_cart_url, :order_key, :flg_stock, :deleted, :company_id, combo_details_attributes: [:product_id, :qty, :subtotal]
	belongs_to :company

	actions :all, :except => [:destroy]

	# scope_to do
	# 	current_admin_user.get_scope_by_role
	# end

	show do
		attributes_table do
	      row :name
	      row :description
	      row :price
	      row :deleted
	      row :flg_stock
	      row :order_key
	      row :img_v2_url do
	      	image_tag(combo.img_v2_url.url(:thumb), :height => '100')
	      end

	      row :img_shopping_cart_url do
	      	image_tag(combo.img_shopping_cart_url.url(:thumb), :height => '100')
	      end
	    end
		panel "Detalles" do
			table_for combo.combo_details do 
			  column("Nombre") {|co| co.product.name} 
			  column "Cantidad", :qty
			  column "Subtotal", :subtotal
			end
		end
	end

	index do
		column("Nombre", :name)
	    column("Descripción", :description)
	    column("Precio", :price)
	    column("Desactivado", :deleted)
	    column("Con Stock", :flg_stock)
	    column("Orden", :order_key)
	    actions
	end

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs 'Detalles' do 
			input :company_id, :as => :hidden, :value => Company.find(company.id).id
			input :name, label: 'Nombre' unless current_admin_user.company_admin
			input :description, label: 'Descripción' unless current_admin_user.company_admin
			input :price, label: 'Precio'  unless current_admin_user.company_admin
			input :deleted, label: 'Deleted'
			input :flg_stock, label: 'Flag Stock' unless current_admin_user.company_admin
			input :order_key, label: 'Orden'
			input :img_v2_url, label: 'Imagen de combo en lista', :as => :file unless current_admin_user.company_admin
			input :img_shopping_cart_url, label: 'Imagen de combo en carrito', :as => :file unless current_admin_user.company_admin
			div do
				render('/admin/calculate_total_button.html.erb', :model => 'combo')  unless current_admin_user.company_admin
			end
			# , input_html: {
		 #      # onchange: remote_get("change_price", 'city_id', :key => "value", )
		 #      onchange :js
		 #    }
		end
		unless current_admin_user.company_admin
			f.inputs do
				f.has_many :combo_details, heading: 'Detalle' do |od|
					od.input :category, :label => 'Category', :as => :select, :collection => Category.all.map{|u| ["#{u.name}", u.id]}
					od.input :brand, :label => 'Brand', :as => :select, :collection => Brand.all
				  od.input :product, :label => 'Product', :as => :select, :collection => Product.all.map { |p| ["#{p.name}", p.id]  }
				  od.input :product_price, :label => 'Precio Unitario', :input_html => { :disabled => true }
				  od.input :qty, label: 'Quantity'
				  od.input :subtotal
				end
			end
		end
		f.actions
	end

	 # member_action :reset_pwd do 
	 #    #yor cool code here
	 #    #remeber you can even access the params hash.
	 #    render :js => "$('.flash_notifications').html('this action rocks')"
	 #  end



end
