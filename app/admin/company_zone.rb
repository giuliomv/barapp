ActiveAdmin.register CompanyZone do
	permit_params :active, :name, area_limits_array: []
	belongs_to :company

	controller do
	  def create
	  	coordinates = JSON.parse(params[:company_zone][:area_limits_array]).to_a
	  	zone = CompanyZone.new
	  	zone.name = params[:company_zone][:name]
	  	zone.active = params[:company_zone][:active]
	  	zone.area_limits = coordinates
	  	zone.company_id = params[:company_id]
	  	if zone.save!
	  		redirect_to admin_company_company_zone_path(params[:company_id],zone.id)
	  	end
	  end

	  def update
	  	coordinates = JSON.parse(params[:company_zone][:area_limits_array]).to_a
	  	zone = CompanyZone.find(params[:id])
	  	zone.area_limits = coordinates
	  	zone.active = params[:company_zone][:active]
	  	zone.name = params[:company_zone][:name]
		zone.save!
		redirect_to admin_company_company_zone_path(params[:company_id],zone.id)
	  end
	end

	index do
		column("Id", :id)
		column("Nombre", :name)
		column("Compañía", :company)
		column("Activado", :active)
		actions
	end

	show do
		attributes_table do
			row :name
			row :active
			render :partial => '/gmaps4rails/mapForCompanyZone.html.erb', :locals => {:zone_points => company_zone.gmaps_zone_points, :read_only => true, :map_center => Company.find(params[:company_id]).location }
		end
	end

	form  do |f|
		f.inputs "Datos de zona" do
			f.input :area_limits_array, :as => :hidden
			f.input :name
			f.input :active 
		end
		if f.object.new_record?
		  render :partial => '/gmaps4rails/mapForCompanyZone.html.erb', :locals => {:zone_points => nil, :read_only => false, :map_center => Company.find(params[:company_id]).location}
		else
		  render :partial => '/gmaps4rails/mapForCompanyZone.html.erb', :locals => {:zone_points => company_zone.gmaps_zone_points, :read_only => false, :map_center => Company.find(params[:company_id]).location}
		end
		f.actions
	end
end
