ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation, :company_id
  includes :company

  index do
    selectable_column
    id_column
    column :email
    column :company
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :company, label: 'Compañía', :as => :select, :collection => Company.all.map{|c| ["#{c.name}", c.id]}
    end
    f.actions
  end

end
