#encoding: utf-8
ActiveAdmin.register Order do
	permit_params :delivery_address, :address_reference, :contact_phone, :total, :contact_name, :state_id, :platform, :fidelity_points

 	actions :all, except: [:create, :destroy] #just show

 	#Filters

 	filter :order_state, :if => proc{current_admin_user.superadmin}
 	filter :company, :if => proc{current_admin_user.superadmin}
 	filter :user, :if => proc{current_admin_user.superadmin}
 	filter :net_promoting_scores, :if => proc{current_admin_user.superadmin}
 	filter :contact_name_contains, :if => proc{current_admin_user.superadmin}
 	filter :delivery_address_contains, :if => proc{current_admin_user.superadmin}
 	filter :contact_phone_contains, :if => proc{current_admin_user.superadmin}
 	filter :total, :if => proc{current_admin_user.superadmin}
 	filter :payment_method, :if => proc{current_admin_user.superadmin}
 	filter :created_at, :if => proc{current_admin_user.superadmin}
 	filter :updated_at, :if => proc{current_admin_user.superadmin}
 	filter :platform, :if => proc{current_admin_user.superadmin}
 	filter :applied_coupon, :if => proc{current_admin_user.superadmin}
 	filter :fidelity_points, :if => proc{current_admin_user.superadmin}
 	filter :order_delivery_fee, :if => proc{current_admin_user.superadmin}

 	filter :user, collection: -> { User.joins(:orders).where(orders:{company:current_admin_user.company}).uniq! },:if => proc{current_admin_user.company_admin}
 	
	controller do
		def index
		  index! do |format|
		  	format.csv{super}
		    format.xls {
		      spreadsheet = OrdersSpreadsheet.new @orders
		      send_data spreadsheet.generate_xls, filename: "orders.xls"
		    }
		  end
		end

		def show
	      @order = Order.find params[:id]
	    end

		def scoped_collection
			if current_admin_user.superadmin
				super.includes :user, :order_state, :company
			else
				super.includes :user, :order_state
			end
		end
	end

	member_action :accept_order do
		message = resource.accept_order
		if message == "venta_exitosa"
			if resource.user and !resource.user.device_tokens.empty?
				push_n = PushNotification.new
				push_n.notification_title = "Bar App"
				push_n.notification_message = "Tu pedido ha sido aceptado y en breve será despachado"
				if push_n.save!
				  data = {order_update_push: true, order_id: resource.id, state_id: 3}
				  dt = DeviceToken.where(tokenable_type: "User", tokenable_id: resource.user_id, device_platform: resource.platform.downcase).first
				  if resource.platform.downcase == DeviceToken::IOS
				    push_n.send_ios_notification(dt, data)
				  else
				    push_n.send_android_notification([dt], data, "android_app")
				  end
				end
			end
			resource.update_attribute(:state_id, 3)
			redirect_to resource_path, notice: "Aceptado!"
		else
			redirect_to resource_path, notice: "Se rechazó el pago con el siguiente mensaje: #{resource.payments.last.message_user}"
		end
	end

	member_action :reject_order do
		resource.reject_order
		redirect_to resource_path, alert: "Rechazado!"
	end

	member_action :in_transit_order do
		resource.in_transit_order
	redirect_to resource_path, notice: "En tránsito!"
	end

	member_action :finalize_order do
		resource.finalize_order
	redirect_to resource_path, notice: "Finalizado!"
	end

	collection_action :new_order_arrived, :method => :post do
		if current_admin_user.company_admin
		  	if current_admin_user.company_id == params[:company_id].to_i
			    orders = Order.where(:company_id => current_admin_user.company_id).order(created_at: :desc).limit(10)
		        flash[:notice] = "Nueva orden!"
			  	render :partial => '/new_orders/orders_list.html.erb', :locals => {:orders => orders }
			else
				render :nothing => true, :status => 204 
		  	end
		elsif current_admin_user.superadmin
			orders = Order.all.order(created_at: :desc).limit(10)
		    flash[:notice] = "Nueva orden!"
		  	render :partial => '/new_orders/orders_list.html.erb', :locals => {:orders => orders }
		else
			render :nothing => true, :status => 204 
		end
	end

  	action_item :accept_order, only:[:show] do
		link_to "Confirmar!", accept_order_admin_order_path if resource.state_id==1
	end
	action_item :reject_order, only:[:show] do
		link_to "Rechazar!", reject_order_admin_order_path if resource.state_id==1
	end
	action_item :in_transit, only:[:show] do
		link_to "En Transito!", in_transit_order_admin_order_path if resource.state_id==3
	end
	action_item :finalize_order, only:[:show] do
		link_to "Finalizado!", finalize_order_admin_order_path if resource.state_id==2
	end

	scope :all, :default => true, if: proc{ current_admin_user.superadmin }
	if proc{ current_admin_user.company_admin }
		scope_to do
			current_admin_user.company
		end
	end
	scope :received do |orders|
		orders.where(:order_state => 1)
	end
	scope :in_transit do |orders|
		orders.where(:order_state => 2)
	end
	scope :accepted do |orders|
		orders.where(:order_state => 3)
	end
	scope :rejected do |orders|
		orders.where(:order_state => 4)
	end
	scope :finalized do |orders|
		orders.where(:order_state => 5)
	end

	index row_class: ->elem { 'not-allowed' if (!elem.user.nil?) and (elem.user.user_age < 18) } do
		column("Id", :id)
		column("Contacto", :contact_name)
	    column("Teléfono", :contact_phone)
	    column("Estado") {|order| status_tag(order.order_state.name) }
	    column("Dirección de entrega", :delivery_address)
	    column("Referencia", :address_reference)
	    column("Método de pago", :payment_method)
	    column("Monto de pago") {|order| number_to_currency order.cash_amount, :unit => "S/. ", seperator: "." unless order.payment_method == "VISA"}
	    column("Cupón") {|order| Coupon.find(order.applied_coupon).code unless order.applied_coupon.nil?}
	    column("Total") {|order| number_to_currency order.total, :unit => "S/. ", seperator: "." }
	    column("Plataforma", :platform)
	    column("Compañía") {|order| order.company.name} if current_admin_user.superadmin
	    column("Fecha", :created_at)
	    column("Mayor de edad"){|order| (order.user.nil? ? "¡Preguntar edad!" : (order.user.user_age >= 18 ? "Sí" : "NO"))}
	    actions
	end
	
	show do
		unless order.user.nil?
			panel "Usuario" do
				raw("Nombre: #{order.user.full_name}<br>
					Puntos acumulados: #{order.user.total_fidelity_points}")
			end
		end

		panel "Resumen" do
			raw("Nombre: #{order.contact_name}<br>
				Dirección: #{order.delivery_address}<br>
				Referencia: #{order.address_reference}<br>
				Ubicación: http://maps.google.com/maps?q=#{order.latitude},#{order.longitude}<br>
				Productos:<br>
				#{order.products_string}<br>
				Combos:<br>
				#{order.combos_string}<br>")
		end
		attributes_table do
				link_to 'Aceptar!', accept_order_admin_order_path
	      row :delivery_address
	      row :order_delivery_fee do |order|
	      	number_to_currency order.order_delivery_fee, :unit => "S/. ", seperator: "."
	      end
	      row :address_reference
	      row :contact_phone
	      row :payment_method
	      row :cash_amount do |order|
	      	number_to_currency order.cash_amount, :unit => "S/. ", seperator: "." unless order.cash_amount == 0
	      end
	      row :fidelity_points
	      row :coupon do |order|
	      	Coupon.find(order.applied_coupon).code unless order.applied_coupon.nil?
	      end
	      row :total do |order|
	      	number_to_currency order.total, :unit => "S/. ", seperator: "."
	      end
	      row :contact_name
	      render :partial => '/gmaps4rails/gmaps4rails.html.erb', :locals => {:location => order.location }
	      row :platform
	      row :order_state
	    end
		panel "Detalles" do
			table_for order.order_details do
			  column "Producto",     :product
			  column "Cantidad", :qty
			  column "Subtotal", :subtotal do |od|
			  	number_to_currency od.subtotal, :unit => "S/. ", seperator: "."	
			  end
			  column("Stock Actual"){|od| od.product.stock_qty}
			end

			table_for order.combo_orders do
				column("Nombre") {|co| co.combo.name} 
				column("Descripción") {|co| co.combo.description} 
				column "Cantidad", :qty
				column "Subtotal", :subtotal do |co|
					number_to_currency co.subtotal, :unit => "S/. ", seperator: "."
				end
			end
		end

		unless order.net_promoting_scores.empty?
			panel "Comentarios" do
				table_for order.net_promoting_scores do
					column "Puntuación", :numeric_score
					column "Comentario", :comment
				end
			end
		end
	end

	form do |f|
		f.inputs 'Detalles' do 
			input :delivery_address, label: 'Entregar en'
			input :address_reference, label: 'Referencia'
			input :contact_phone, label: 'Teléfono'
			input :total, label: 'Total'
			input :contact_name, label: 'Contacto'
			input :order_state, label: 'Estado'
			input :platform, label: 'Plataforma', :as => :select, :collection => [["Android", "Android"], ["iOS", "iOS"], ["Llamada", "Llamada"], ["Whatsapp", "Whatsapp"]]
		end
		f.inputs do
			f.has_many :order_details, heading: 'Detalles' do |od|
			  od.input :product, :input_html => { :disabled => true }
			  od.input :qty, label: 'Quantity', :input_html => { :disabled => true }
			  od.input :subtotal, :input_html => { :disabled => true }
			end
		end
		f.inputs do
			f.has_many :combo_orders, heading: 'Combos' do |co|
			  co.input :combo, :input_html => { :disabled => true }  
			  co.input :qty, label: 'Quantity', :input_html => { :disabled => true }
			  co.input :subtotal, :input_html => { :disabled => true }
			end
		end
		f.actions
	end


end
