ActiveAdmin.register User do
	
	permit_params :id, :email, :full_name, :fb_link, :phone_number, :fb_gender, :fb_picture, :fb_birthday, :total_fidelity_points

	controller do
		def index
		  index! do |format|
		  	format.csv{super}
		    format.xls {
		      spreadsheet = UsersSpreadsheet.new @users
		      send_data spreadsheet.generate_xls, filename: "users.xls"
		    }
		  end
		end
	end

	action_item only: :index do
	  link_to 'Descargar Excel de Usuario', "/users.xls"
	end

	index do
		column("Id", :id)
	    column("Nombre", :full_name)
	    column("Email", :email)
	    column "Avatar" do |user|
		  image_tag user.fb_picture
		end
	    column("Teléfono", :phone_number)
	    column("Género", :fb_gender)
	    column("Cumpleaños") {|user| DateTime.parse(user.fb_birthday).strftime("%B %d, %Y")}
	    column("Puntos acumulados", :total_fidelity_points)
	    column "Link de FB" do |user|
		  link_to("#{user.full_name}", user.fb_link)
		end
	    actions
	end

	show do
		attributes_table do
			row :full_name
			row :email
			row "Link de FB" do |post|
				link_to("#{user.full_name}", user.fb_link)
			end
			row :phone_number
			row :fb_gender
			row('Puntos de Lealtad') { |user|  user.total_fidelity_points }
			row "Cumpleaños" do |user|
				DateTime.parse(user.fb_birthday).strftime("%B %d, %Y")
			end
			row "Avatar" do |user|
				image_tag user.fb_picture
			end
	    end
		# panel "Likes de #{user.full_name}" do
		# 	raw("#{user.get_user_likes.map { |pa| pa["name"]  }.join("<br>")}")
		# end
		# panel "Detalles" do
		# 	table_for order.order_details do
		# 	  column "Producto", :product
		# 	  column "Cantidad", :qty
		# 	  column "Subtotal", :subtotal do |od|
		# 	  	number_to_currency od.subtotal, :unit => "S/. ", seperator: "."	
		# 	  end
		# 	  column("Stock Actual"){|od| od.product.stock_qty}
		# 	end
		# end
	end

	form do |f|
	    f.inputs "Detalles de Usuario" do
	      f.input :total_fidelity_points
	    end
    f.actions
  end

end
