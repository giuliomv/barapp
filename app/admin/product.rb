ActiveAdmin.register Product do
	permit_params :brand_product_id, :unitPrice, :stock_qty, :flg_stock, :deleted, :company_id, :order_key
	belongs_to :company
	includes :brand_product

	actions :all, :except => [:destroy]

	controller do
		def find_resource
			Product.find(params[:id])
	    end

		def update
	      super do |format|
	        redirect_to admin_company_product_url and return if resource.valid?
	      end
	    end
	end

	csv do
		column("Nombre") {|product| product.brand_product.name unless product.brand_product.nil?}
	 	column("Marca") {|product| product.brand_product.brand.name unless product.brand_product.nil?}
	    column("Precio unitario"){|product| product.unitPrice}
	    column("Descripción") {|product| product.brand_product.description unless product.brand_product.nil?}
	    column("Desactivado"){|product| product.deleted}
	    column("Orden"){|product| product.order_key}
	    column("Con Stock"){|product| product.flg_stock}
	    column("Stock"){|product| product.stock_qty}
	    column("Categoría"){|product| product.brand.category.name unless product.brand_product.nil?}
	end

	scope :all, :default => true

	scope :whiskys do |products|
		products.by_category(1)
	end

	scope :piscos do |products|
		products.by_category(2)
	end

	scope :vodkas do |products|
		products.by_category(3)
	end

	scope :rones do |products|
		products.by_category(4)
	end

	scope :cervezas do |products|
		products.by_category(5)
	end

	scope :otros do |products|
		products.by_category(6)
	end

	scope :gaseosas do |products|
		products.by_category(7)
	end

	scope :cigarros do |products|
		products.by_category(8)
	end

	scope :energizantes do |products|
		products.by_category(9)
	end

	scope :piqueos do |products|
		products.by_category(10)
	end

	scope :extras do |products|
		products.by_category(11)
	end

	scope :vinos do |products|
		products.by_category(13)
	end

	scope :gines do |products|
		products.by_category(14)
	end


	collection_action :get_product_by_id, method: :get do
		products = Product.where('id = ?', "#{params[:product_id]}")
		render json: products.first, root: false
	end

	collection_action :get_products_by_brand_id, method: :get do
		products = Product.joins(:brand_product => :brand).where(:brand_products=>{:brand_id=>params[:brand_id]},:company_id=>params[:requested_company_id])
		render json: products, root: false, :methods => [:name, :description, :product_img_v2_url, :brand_id]
	end

	collection_action :reset_stock, :method => :post do
		Product.where(company_id:params[:company_id]).update_all(:flg_stock => true, :deleted => false, :stock_qty => 20)
		redirect_to collection_path(:company_id => params[:company_id]), notice: "Stock Reiniciado!"
	end

	action_item only: :index, if: proc{ current_admin_user.superadmin } do
		link_to 'Reiniciar stock', reset_stock_admin_company_products_path, method: :post
	end

	index do
		column("Nombre") {|product| product.brand_product.name unless product.brand_product.nil?}
	 	column("Marca") {|product| product.brand_product.brand.name unless product.brand_product.nil?}
	    column("Precio unitario", :unitPrice)
	    column("Descripción") {|product| product.brand_product.description unless product.brand_product.nil?}
	    column("Desactivado", :deleted)
	    column("Orden", :order_key)
	    column("Con Stock", :flg_stock)
	    column("Stock", :stock_qty)
	    actions
	end

	show do
		attributes_table do
			row "Nombre" do |product|
				product.name
			end
			row "Descripción" do |product|
				product.description
			end
			row "Marca" do |product|
				product.brand.name unless product.brand.nil?
			end
			row :unitPrice
			row :flg_stock
			row :stock_qty
			row :deleted
			row :order_key
			row :img_v2_url do
				image_tag(product.brand_product.img_v2_url.url(:thumb), :height => '100' ) unless product.brand_product.nil?
			end
	    end
	end

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs 'Detalles' do 
			input :brand_product, label: 'Marca', :as => :select, :collection => BrandProduct.all.map{|u| ["(#{u.brand.category.name.upcase}) - #{u.name}", u.id]}  unless current_admin_user.company_admin
			input :unitPrice, label: 'Precio unitario' unless current_admin_user.company_admin
			input :stock_qty, label: 'Stock' unless current_admin_user.company_admin
			input :flg_stock, label: 'Flag stock' unless current_admin_user.company_admin
			input :deleted, label: 'Deleted'
			input :order_key, label: 'Orden' unless current_admin_user.company_admin
		end
		
		f.actions
	end


end
