ActiveAdmin.register Company do

  permit_params :name, :address, :phone, :ruc, :contact_id_doc, :contact_name, :latitude, :longitude, :highest_delivery_fee, :minimum_amount, :deleted
  includes :admin_users

  filter :name, :if => proc{current_admin_user.superadmin}
  filter :deleted, :if => proc{current_admin_user.superadmin}

  form do |f|
    f.inputs "Datos de la compañía" do
      f.input :name, label: 'Razón Social'
      f.input :address, label: 'Dirección'
      f.input :phone, label: 'Teléfono'
      f.input :ruc, label: 'RUC'
      f.input :contact_name, label: 'Nombre de contacto'
      f.input :contact_id_doc, label: 'ID del contacto'
      f.input :highest_delivery_fee, label: 'Tasa de delivery máxima'
      f.input :latitude, :input_html => { :readonly => true }
      f.input :longitude , :input_html => { :readonly => true }
      f.input :minimum_amount, label: 'Monto mínimo'
      f.input :deleted, label: 'Deleted'
    end
    if f.object.new_record?
      render :partial => '/gmaps4rails/mapForCompanyRegistration.html.erb', :locals => {:loc => nil}
    else
      render :partial => '/gmaps4rails/mapForCompanyRegistration.html.erb', :locals => {:loc => company.location}
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :address
      row :phone
      row :ruc
      row :contact_name
      row :contact_id_doc
      row('Administrador') { |company|  company.admin_users.first.email unless company.admin_users.empty?} 
      row :highest_delivery_fee
      row :minimum_amount
      render :partial => '/gmaps4rails/gmaps4rails.html.erb', :locals => {:location => company.location }
      #row(:location) {|o| gmaps("markers" => {data: o.location}, "map_options" =>  { auto_zoom: false, zoom: 15 }) }
    end

    panel "Banners" do    
      table_for company.banner_entities do
        column "Nombre de banner", :name
        column "Tipo de banner", :banner_type
        column "Orden", :order_key
        column do |p| 
          if p.banner_type=="Ad"
            link_to "Ver Publicidad", admin_company_company_banner_ad_path(p.company_id, p.id) 
          else
            link_to "Ver Combo", admin_company_combo_path(p.company_id, p.id) 
          end
        end
      end
    end
  end

  index do
    column("Razón Social", :name)
    column("Dirección", :address)
    column("Teléfono", :phone)
    column("Nombre de contacto", :contact_name)
    column("Usuario") {|company| company.admin_users.first.email unless company.admin_users.empty?} 
    column("Tasa de delivery máxima (S/.)", :highest_delivery_fee)
    column("Monto mínimo (S/.)", :minimum_amount)
    column "Productos" do |company|
      link_to "Productos de #{company.name}",  admin_company_products_path(company)
    end
    column "Combos" do |company|
      link_to "Combos de #{company.name}",  admin_company_combos_path(company)
    end
    column "Horarios de atención" do |company|
      link_to "Horarios de atención de #{company.name}",  admin_company_active_schedules_path(company)
    end
    column "Texto para horario" do |company|
      link_to "Texto para horario de #{company.name}",  admin_company_schedule_texts_path(company)
    end
    column "Capas de delivery" do |company|
      link_to "Capas de #{company.name}",  admin_company_delivery_tiers_path(company)
    end
    column "Zonas de entrega" do |company|
      link_to "Zonas de #{company.name}",  admin_company_company_zones_path(company)
    end
    column "Banners" do |company|
      link_to "Banners de #{company.name}",  admin_company_company_banner_ads_path(company)
    end
    actions
  end

end
