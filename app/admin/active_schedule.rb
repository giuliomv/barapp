ActiveAdmin.register ActiveSchedule do
	permit_params :day_of_week, :initial_hour, :initial_minutes, :end_hour, :end_minutes, :initial_time, :end_time
	belongs_to :company

	before_create do |active_schedule|
		active_schedule.company_id = params[:company_id]
	end


	index do
		column("Día de la semana") {|active_schedule| Hash[ActiveSchedule::DAYS_OF_WEEK.map{|key, value| [value, key]}][active_schedule.day_of_week]}
		column("Hora de inicio") {|active_schedule| "#{"%02d" % active_schedule.initial_hour}:#{"%02d" % active_schedule.initial_minutes}"}
		column("Hora de fin") {|active_schedule| "#{"%02d" % active_schedule.end_hour}:#{"%02d" % active_schedule.end_minutes}"}
		actions
	end

	form do |f|
		f.inputs 'Detalles' do 
			input :day_of_week, :label => 'Día', :as => :select, :collection => ActiveSchedule::DAYS_OF_WEEK
			if f.object.new_record?
				input :initial_time, label: 'Hora de inicio', :as => :time_picker
				input :end_time, label: 'Hora de fin', :as => :time_picker
			else
				input :initial_time, label: 'Hora de inicio', :as => :time_picker, :input_html => { :value => "#{"%02d" % active_schedule.initial_hour}:#{"%02d" % active_schedule.initial_minutes}" }
				input :end_time, label: 'Hora de fin', :as => :time_picker, :input_html => { :value => "#{"%02d" % active_schedule.end_hour}:#{"%02d" % active_schedule.end_minutes}" }
			end
		end
		f.actions
	end

end
