ActiveAdmin.register Brand do

	permit_params :name, :category_id, :banner_image_url
	includes :category

	collection_action :get_brands_by_category_id, method: :get do
		brands = Brand.where('category_id = ?', "#{params[:category_id]}")
		render json: brands, root: false
	end

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs 'Detalles' do 
			input :category, label: 'Categoría', :as => :select, :collection => Category.all.map{|u| ["#{u.name}", u.id]}
			input :name, label: 'Nombre'
			input :description, label: 'Descripción'
			input :banner_image_url, label: 'Imagen de Banner', :as => :file
		end
		f.actions
	end

	show do
		attributes_table do
	      row :name
	      row :category
	      row :banner_image_url do
	      	image_tag(brand.banner_image_url.url(:thumb), :height => '100')
	      end
	    end
	end

	index do
		column("Nombre", :name)
	    column("Categoría", :category)
	    actions
	end

end
