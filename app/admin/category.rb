ActiveAdmin.register Category do
	
	permit_params :name, :img_code, :deleted, :description, :order_key, :banner_image_url, :alcoholic

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs 'Detalles' do 
			input :name, label: 'Nombre'
			input :img_code, label: 'Código'
			input :description, label: 'Descripción'
			input :order_key, label: 'Orden'
			input :banner_image_url, label: 'Imagen de Banner', :as => :file
			input :deleted, label: 'Deleted'
			input :alcoholic, label: '¿Es Alcohólica?'
		end
		
		f.actions
	end

	show do
		attributes_table do
	      row :name
	      row :description
	      row :img_code
	      row :order_key
	      row :deleted
	      row :alcoholic
	      row :banner_image_url do
	      	image_tag(category.banner_image_url.url(:thumb), :height => '100')
	      end
	    end
	end
end
