ActiveAdmin.register BannerAd do
	permit_params :name, :img_url

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs 'Detalles' do 
			input :name, label: 'Nombre'
			input :img_url, label: 'Imagen de Banner', :as => :file
		end
		f.actions
	end

	show do
		attributes_table do
	      row :name
	      row :img_url do
	      	image_tag(banner_ad.img_url.url(:thumb), :height => '100')
	      end
	    end
	end

end
