ActiveAdmin.register ScheduleText do
	permit_params :current_text
	belongs_to :company
	# scope_to do
	# 	current_admin_user.get_scope_by_role
	# end

	before_create do |schedule_text|
		schedule_text.company_id = params[:company_id]
	end

end
