class UsersSpreadsheet
  attr_accessor :users
  
  def initialize users
    @users = users
  end

  def generate_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: "Users"

    create_body sheet

    data_to_send = StringIO.new
    book.write data_to_send
    data_to_send.string 
  end

  def generate_and_store_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: "Users"

    create_body sheet

    book.write 'public/users.xls' 
  end

  def create_body sheet
    sheet.row(0).concat %w{Id Nombre Like}
    sheet.row(0).default_format = Spreadsheet::Format.new weight: :bold

    row_index = 1
    User.get_users_likes_batch(users).each do |u_likes|
      sheet.row(row_index).concat [u_likes[:user_id], u_likes[:user_name], u_likes[:like_name]]
      row_index += 1
    end
  end
end