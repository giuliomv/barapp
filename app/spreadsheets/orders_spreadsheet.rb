class OrdersSpreadsheet
  attr_accessor :orders
  
  def initialize orders
    @orders = orders
  end

  def generate_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: "Órdenes"

    create_body sheet

    data_to_send = StringIO.new
    book.write data_to_send
    data_to_send.string 
  end

  def create_body sheet
    sheet.row(0).concat %w{Año Mes Licorería IdBoleta Fecha Hora NombreCompleto Producto Descripción Categoría Cantidad PrecioVenta PrecioTotal Plataforma Captación Delivery Cupón Distrito DsctoCupón Telefono Canal Recomendación Comentario Estado MétodoDePago Latitud Longitud}
    sheet.row(0).default_format = Spreadsheet::Format.new weight: :bold

    row_index = 1
    @orders.each do |o|
      o.order_details.each do |od|
        sheet.row(row_index).concat [o.created_at.year, o.created_at.month, o.company.name, o.id, o.created_at.strftime("%d/%m/%y"), o.created_at.strftime("%R"), o.contact_name, od.product.name, od.product.description, od.product.brand.category.name, od.qty, od.subtotal, o.total, o.platform, "", o.order_delivery_fee, (o.applied_coupon.nil? ? "" : Coupon.find(o.applied_coupon).code), "", (o.applied_coupon.nil? ? "" : "#{Coupon.find(o.applied_coupon).discount_amount}%"), o.contact_phone, "", "", "", o.order_state.name, o.payment_method, o.latitude, o.longitude]
        row_index += 1
      end
      o.combo_orders.each do |co|
        sheet.row(row_index).concat [o.created_at.year, o.created_at.month, o.company.name, o.id, o.created_at.strftime("%d/%m/%y"), o.created_at.strftime("%R"), o.contact_name, co.combo.name, co.combo.description, "", co.qty, co.subtotal, o.total, o.platform, "", o.order_delivery_fee, (o.applied_coupon.nil? ? "" : Coupon.find(o.applied_coupon).code), "", (o.applied_coupon.nil? ? "" : "#{Coupon.find(o.applied_coupon).discount_amount}%"), o.contact_phone, "", "", "", o.order_state.name, o.payment_method, o.latitude, o.longitude]
        row_index += 1
      end
    end
  end
end