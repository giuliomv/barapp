class Payment < ActiveRecord::Base
	belongs_to :culqi_cards
	belongs_to :order
	validates :amount, numericality: {greater_than_or_equal_to: 100}

	def self.create_charge(order)
		Culqi.public_key = CULQUI_PUBLIC_KEY
		Culqi.secret_key = CULQUI_SECRET_KEY
		order = Order.find(order.id)
		culqi_card = CulqiCard.find(order.card_id_selected)
		charge = Culqi::Charge.create(
		    :amount => (order.total * 100).to_s.split(".").first,
		    :capture => true,
		    :currency_code => 'PEN',
		    :description => "Venta de #{order.user.full_name}",
		    :installments => 0,
		    :email => order.user.email,
		    :source_id => culqi_card.id_card
		)
		p "#{JSON.parse(charge)}"
		JSON.parse(charge)
	end
end
