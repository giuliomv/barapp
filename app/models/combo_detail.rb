class ComboDetail < ActiveRecord::Base
  belongs_to :combo
  belongs_to :product

  attr_accessor :brand
  attr_accessor :category
  attr_accessor :product_price
end
