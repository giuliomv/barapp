class Authentication < ActiveRecord::Base
	FACEBOOK_APP_ID = "570387569827053"
	FACEBOOK_APP_SECRET = "44a735796fc4a8a04ee62abba70ce9c2"
	belongs_to :authenticable, :polymorphic => true
	# validates_presence_of :uid, :provider, :oauth_token
	validates_uniqueness_of :uid, scope: :provider

	def renew_token
		new_token = Koala::Facebook::OAuth.new(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET).exchange_access_token(self.oauth_token)
	  	self.oauth_token = new_token['access_token']
	  	self.oauth_token_expires_at = Time.now + new_token['expires'].to_i.seconds
	  	self.save
	end

	def token_to_expire?
		self.oauth_token_expires_at < (Time.now - 2.day)
	end

end
