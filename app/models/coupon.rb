class Coupon < ActiveRecord::Base
	validates :code, uniqueness: true
	before_create :setup_redemptions_count
	validates_numericality_of :discount_amount,
        greater_than_or_equal_to: 0,
        less_than_or_equal_to: 100

	def self.is_valid?(input_code)
		coupon = self.find_by(:code => input_code)
		if coupon
			!coupon.is_stockout? and !coupon.is_expired? and !coupon.is_not_ready?
		else
			false
		end
	end

	def is_stockout?
		self.redemptions_count >= self.redemptions_limit
	end

	def is_expired?
		self.valid_until.to_date < DateTime.now.to_date
	end

	def is_not_ready?
		self.valid_from.to_date > DateTime.now.to_date 
	end

	def redeem
		self.redemptions_count += 1
		self.save
	end
	
	protected
		def setup_redemptions_count
		  self.redemptions_count = 0
		end

end
