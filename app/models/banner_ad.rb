class BannerAd < ActiveRecord::Base
	has_many :company_banner_ads
	has_attached_file :img_url, styles: { thumb: "100x100>" }
	validates_attachment_content_type :img_url, content_type: /\Aimage\/.*\Z/

	def img_url_paper_clip_route
  		img_url.url()
  	end

  	def banner_ad_img_url
		if self.img_url.exists?
			img_url.url()
		else
			""
		end
	end
end
