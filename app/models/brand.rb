class Brand < ActiveRecord::Base
	belongs_to :category
	has_many :brand_products

	scope :visual, -> {where.not(banner_image_url_file_name: nil)} 

  	has_attached_file :banner_image_url, styles: { thumb: "100x100>" }
	validates_attachment_content_type :banner_image_url, content_type: /\Aimage\/.*\Z/

	def brand_banner_img_url
		if banner_image_url.exists?
			banner_image_url.url()
		else
			""
		end
	end
end
