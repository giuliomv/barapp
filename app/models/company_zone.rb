class CompanyZone < ActiveRecord::Base
	belongs_to :company
	serialize :area_limits
	attr_accessor :area_limits_array

	def zone_polygon
		Geokit::Polygon.new(self.area_limits.map{|a| Geokit::LatLng.new(a[0], a[1])})
	end

	def point_in_polygon?(lat, lng)
		zone_polygon.contains?(Geokit::LatLng.new(lat, lng))
	end

	def gmaps_zone_points
		self.area_limits.map {|point|  {lat:point[0], lng:point[1]}}
	end
end
