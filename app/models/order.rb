class Order < ActiveRecord::Base
  AMOUNT_PER_POINT = 10

  acts_as_mappable  :default_units => :kms,
            :lat_column_name => :latitude,
            :lng_column_name => :longitude

  belongs_to :order_state, :foreign_key => 'state_id'
  belongs_to :company
  belongs_to :user
  has_many :order_details
  has_many :combo_orders
  has_many :net_promoting_scores
  has_many :payments


  accepts_nested_attributes_for :order_details
  accepts_nested_attributes_for :combo_orders

  def location
    [self.latitude, self.longitude]
  end

  def send_order_received_mail
    unless self.user.nil?
      SendEmailJob.set(wait: 10.seconds).perform_later(self.user, self)
    end
  end

  def send_notification_to_admin
    push_n = PushNotification.new
    push_n.notification_title = "Bar App"
    push_n.notification_message = "Hola #{self.company.name}, tienes un nuevo pedido!"
    if push_n.save!
      data = {order_id: self.id}
      if self.company.admin_users.map{|a| !a.device_tokens.empty?}.any?
        dt = self.company.admin_users.map{|a| a.device_tokens}.flatten
        push_n.send_android_notification(dt, data, "android_mobile_admin_app")
      end
      if AdminUser.where(role:"superadmin").map{|a| !a.device_tokens.empty?}.any?
        dt_sa = AdminUser.where(role:"superadmin").map{|a| a.device_tokens}.flatten
        push_n.send_android_notification(dt_sa, data, "android_mobile_admin_app")
      end
    end
  end

  def self.gmaps_hash(parameters)
    orders = Order.all
    unless parameters.nil?
      orders = orders.where('created_at >= :begin_date', :begin_date => ApplicationController.get_local_date_from_string(parameters[:begin_date], 0)) unless parameters[:begin_date].nil?
      orders = orders.where('created_at <= :end_date', :end_date => ApplicationController.get_local_date_from_string(parameters[:end_date], 1)) unless parameters[:end_date].nil?
      orders = orders.where('company_id = :company_id', :company_id => parameters[:order_company].to_i) unless parameters[:order_company].nil?
      orders = orders.where('state_id = :order_state', :order_state => parameters[:order_state].to_i) unless parameters[:order_state].nil?
    end
    @hash = Gmaps4rails.build_markers(orders) do |order, marker|
      marker.lat order.latitude
      marker.lng order.longitude
      marker.title "#{order.id}"
      marker.infowindow "#{order.created_at}"
      marker.picture ({
                      :url    => ActionController::Base.helpers.asset_path("pin.png"),
                      :width  => "14",
                      :height => "38",
                      :scaledWidth => "7", # Scaled width is half of the retina resolution; optional
                      :scaledHeight => "19", # Scaled width is half of the retina resolution; optional
                     })
    end
    @hash
  end

  def gmaps4rails_address
    "#{self.latitude}, #{self.longitude}" 
  end

  def products_string
    products = ""
    self.order_details.each do |od|
      products += "#{od.product.name} (#{od.product.brand_product.description}): #{od.qty} unidades<br>"
    end
    products
  end

  def combos_string
    combos = ""
    self.combo_orders.each do |co|
      combos += "#{co.combo.name} (#{co.combo.description}): #{co.qty} unidades<br>"
    end
    combos
  end

  def company_name
    self.company.name
  end

  def attach_coupon(coupon)
    self.total = self.total * (100 - coupon.discount_amount) * 0.01
    self.applied_coupon = coupon.id
    self.save
  end

  def include_delivery(delivery_fee)
    self.order_delivery_fee = delivery_fee.to_f
    self.total += delivery_fee.to_f
    self.save
  end

  def accept_order
    if self.state_id==1
      if self.update_attribute(:state_id, 6)
        self.assign_points_to_user
        reduce_stock
        if self.payment_method == "online"
          charge = Payment.create_charge(self)
          payment = Payment.new
          if charge["object"] == "error"
            payment.message_user = charge["user_message"]
            payment.amount = 100
            payment.message_merchant = charge["merchant_message"]
            payment.response_code = charge["type"]
            payment.id_charge = charge["charge_id"]
          else
            payment.message_user = charge["outcome"]["user_message"]
            payment.amount = charge["amount"]
            payment.message_merchant = charge["outcome"]["merchant_message"]
            payment.response_code = charge["outcome"]["type"]
            payment.id_charge = charge["id"]
          end
          payment.card_id = self.card_id_selected
          payment.order_id = self.id
          payment.save
          payment.response_code
        else
          self.update_attribute(:state_id, 3)
          "venta_exitosa"
        end
      end
    end
  end

  def in_transit_order
    if self.state_id==3
      if self.user and !self.user.device_tokens.empty?
        push_n = PushNotification.new
        push_n.notification_title = "Bar App"
        push_n.notification_message = "Tu pedido #{self.id} ya está en camino, ve preparando a la gente!"
        if push_n.save!
          data = {order_update_push: true, order_id: self.id, state_id: 2}
          dt = DeviceToken.where(tokenable_type: "User", tokenable_id: self.user_id, device_platform: self.platform.downcase).first
          if self.platform.downcase == DeviceToken::IOS
            push_n.send_ios_notification(dt, data)
          else
            push_n.send_android_notification([dt], data, "android_app")
          end
        end
      end
      self.update_attribute(:state_id, 2)
    end
  end

  def reject_order
    if self.state_id==1
      if self.user and !self.user.device_tokens.empty?
        push_n = PushNotification.new
        push_n.notification_title = "Bar App"
        push_n.notification_message = "¡Lo sentimos! Tu pedido no podrá ser entregado :("
        if push_n.save!
          data = {order_update_push: true, order_id: self.id, state_id: 4}
          dt = DeviceToken.where(tokenable_type: "User", tokenable_id: self.user_id, device_platform: self.platform.downcase).first
          if self.platform.downcase == DeviceToken::IOS
            push_n.send_ios_notification(dt, data)
          else
            push_n.send_android_notification([dt], data, "android_app")
          end
        end
      end
      self.update_attribute(:state_id, 4)
    end
  end

  def finalize_order
    if self.state_id==2
      if self.user and !self.user.device_tokens.empty?
        push_n = PushNotification.new
        push_n.notification_title = "Bar App"
        push_n.notification_message = "Hola #{self.user.first_name}, califica tu experiencia de compra con Bar App"
        if push_n.save!
          data = {nps_push: true, order_id: self.id}
          dt = DeviceToken.where(tokenable_type: "User", tokenable_id: self.user_id, device_platform: self.platform.downcase).first
          if self.platform.downcase == DeviceToken::IOS
            push_n.send_ios_notification(dt, data)
          else
            push_n.send_android_notification([dt], data, "android_app")
          end
        end
      end
      self.update_attribute(:state_id, 5)
    end
  end

  def calculate_fidelity_points
    self.fidelity_points = (self.total / AMOUNT_PER_POINT).round
    self.save
  end

  def assign_points_to_user
    if self.user
      self.user.total_fidelity_points += self.fidelity_points
      self.user.save
    end
  end

  def current_state
    self.order_state.name
  end

  private 
    def reduce_stock
      self.order_details.each do |order_detail|
        product = order_detail.product
        new_stock = product.stock_qty - order_detail.qty
        product.update_attribute(:stock_qty, new_stock)
        if product.stock_qty < 1
          product.flg_stock = false
          product.save
        end
      end
      self.combo_orders.each do |combo_order|
        combo_order.combo.combo_details.each do |c_detail|
          p = c_detail.product
          new_stock = p.stock_qty - c_detail.qty*combo_order.qty
          p.update_attribute(:stock_qty, new_stock)
          if p.stock_qty < 1
            p.flg_stock = false
            p.save
            c = combo_order.combo
            c.flg_stock = false
            c.save
          end
        end
      end
    end

end
