class Combo < ActiveRecord::Base
    include SearchCop
  	has_many :combo_orders
  	has_many :combo_details
  	belongs_to :company

  	accepts_nested_attributes_for :combo_details

  	has_attached_file :img_url, styles: { thumb: "100x100>" }
  	has_attached_file :img_v2_url, styles: { thumb: "100x100>" }
    has_attached_file :img_shopping_cart_url, styles: { thumb: "100x100>" }
  	validates_attachment_content_type :img_url, content_type: /\Aimage\/.*\Z/
  	validates_attachment_content_type :img_v2_url, content_type: /\Aimage\/.*\Z/
    validates_attachment_content_type :img_shopping_cart_url, content_type: /\Aimage\/.*\Z/

    search_scope :search do
      attributes :name, :description
    end


  	def img_url_paper_clip_route
  		img_url.url()
  	end

  	def img_v2_url_paper_clip_route
  		img_v2_url.url()
  	end

    def img_shopping_cart_url_paper_clip_route
      img_shopping_cart_url.url()
    end

    def banner_type
      "Combo"
    end
end
