class PushNotification < ActiveRecord::Base
	attr_accessor :target_users

	def send_notification_to_all
		if self.to_ios
			create_all_ios_notification
		end
		if self.to_android
			create_all_android_notification
		end
	end

	def send_notifications_to_users(target_users)
		android_tokens = []
		target_users.each do |user_id|
			user = User.find(user_id)
			user.device_tokens.each do |dt|
				if dt.device_platform == DeviceToken::IOS
					send_ios_notification(dt, {})
				else
					android_tokens.push(dt)
				end
			end
		end
		send_android_notification(android_tokens, {}, "android_app")
	end

	def create_all_ios_notification
		data = {message: self.notification_message}
		DeviceToken.where(:device_platform => DeviceToken::IOS).each do |d|
			send_ios_notification(d, data)
		end
	end

	def create_all_android_notification
		tokens = DeviceToken.where(:device_platform => DeviceToken::ANDROID)
		send_android_notification(tokens, {}, "android_app")
	end

	def send_ios_notification(token, data)
		n = Rpush::Apns::Notification.new
		n.app = Rpush::Apns::App.find_by_name("ios_app_new")
		n.device_token = "#{token.token_string}"
		data[:message] = self.notification_message
		n.data = data
		n.alert = "#{self.notification_message}"
		n.save!
	end

	def send_android_notification(tokens, data, app_name)
		i = 0
		token_ids = tokens.map { |dt| dt.token_string }
		while token_ids.count > (900*i) do
			i += 1
			n = Rpush::Gcm::Notification.new
			data[:title] = self.notification_title
			data[:message] = self.notification_message
			n.data = data
			n.app = Rpush::Gcm::App.find_by_name(app_name)
			n.priority = 'high'
			n.content_available = true
			tokens_to_send = token_ids[(900*i-900)..((900*i)-1)] 
			n.registration_ids = tokens_to_send
			n.save!
		end
	end

end
