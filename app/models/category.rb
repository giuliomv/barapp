class Category < ActiveRecord::Base
	has_many :visual_brands,-> { where.not(banner_image_url_file_name: nil) }, class_name: 'Brand'
	has_many :brands
	has_attached_file :banner_image_url, styles: { thumb: "100x100>" }
	validates_attachment_content_type :banner_image_url, content_type: /\Aimage\/.*\Z/

	def category_banner_img_url
		if banner_image_url.exists?
			banner_image_url.url()
		else
			""
		end
	end

	def self.with_products_available(company_id)
		ids = Category.includes(brands: [brand_products:[:products]]).where("products.company_id = ? AND products.deleted = ? AND products.stock_qty > 0 AND products.flg_stock = ?", company_id, false, true).group("categories.id").count("products.id").keys
		find(ids)
	end
end