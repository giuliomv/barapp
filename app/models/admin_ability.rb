class AdminAbility
  include CanCan::Ability

  def initialize(admin)
    # Define abilities for the passed in user here. For example:

    if admin.role == 'superadmin'
        can :manage, :all
    else
        # can :show, Company, :id => admin.companies.first.id
        can :read, ActiveAdmin::Page, :name => "Ordenes Nuevas"
        can :manage, Product, :company_id => admin.company_id
        can :manage, Combo, :company_id => admin.company_id
        can [:read, :accept_order, :reject_order, :in_transit_order, :finalize_order], Order, :company_id => admin.company_id
        can :manage, ActiveSchedule, :company_id => admin.company_id
        can :manage, ScheduleText, :company_id => admin.company_id
        can :read, ActiveAdmin::Page, :name => "Dashboard"
    end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
