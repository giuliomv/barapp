class CulqiCard < ActiveRecord::Base
	has_many :payments
	belongs_to :user

	def self.create_remote_card(user, address, address_city, token_id)
		customer = nil
		if user.id_customer.nil?
			customer = self.create_customer(user, address, address_city)
			user.id_customer = customer['id']
			user.save
		end
		if ((customer.nil?) or (!customer.nil? and (customer["object"] != "error")))
			Culqi.public_key = CULQUI_PUBLIC_KEY
			Culqi.secret_key = CULQUI_SECRET_KEY
			card = Culqi::Card.create(
			      :customer_id => user.id_customer,
			      :token_id => token_id,
			      :validate => true
			)
			JSON.parse(card)
		else
			customer
		end
	end

	def self.create_customer(user, address, address_city)
		Culqi.public_key = CULQUI_PUBLIC_KEY
		Culqi.secret_key = CULQUI_SECRET_KEY
		customer = Culqi::Customer.create(
		    :address => address,
		    :address_city => address_city,
		    :country_code => "PE",
		    :email => user.email,
		    :first_name => user.first_name,
		    :last_name => user.last_name,
		    :phone_number => user.phone_number
		)
		JSON.parse(customer)
	end
end
