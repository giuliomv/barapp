class CompanyBannerAd < ActiveRecord::Base
	belongs_to :company
	belongs_to :banner_ad

	def banner_type
     	"Ad"
    end

    def name
    	self.banner_ad.name unless self.banner_ad.nil?
    end

    def img_url
    	self.banner_ad.banner_ad_img_url
    end
end
