class Product < ActiveRecord::Base
	include SearchCop
	belongs_to :company
	belongs_to :brand_product
	has_many :order_details
	scope :by_category, ->(category_id) { joins(brand_product: {brand: :category}).where(brands: {category_id:category_id}) }

	search_scope :search do
	    attributes brand_product: "brand_product.name"
	    attributes brand: "brand.name"
	    attributes category: "category.name"
	    scope {joins(brand_product: {brand: :category})}   
	end

	def product_img_v2_url
		if brand_product.img_v2_url.exists?
			brand_product.img_v2_url.url()
		else
			""
		end
	end

	def brand
		brand_product.brand unless brand_product.nil?
	end

	def brand_id
		brand_product.brand_id unless brand_product.nil?
	end

	def category_id
		brand_product.brand.category_id unless brand_product.nil?
	end

	def name
		brand_product.name unless brand_product.nil?
	end

	def description
		brand_product.description unless brand_product.nil?
	end

end
