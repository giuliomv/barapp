class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :company
  has_many :authentications, as: :authenticable
  has_many :device_tokens, as: :tokenable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

	def get_scope_by_role
		if self.role == 'superadmin'
			nil
		else
			self.company
		end
	end

	def ensure_authentication_token
		if self.authentication_token.blank?
		  self.authentication_token = generate_authentication_token
		end
	end

	def superadmin
		self.role == 'superadmin'
	end

	def company_admin
		self.role == 'admin'
	end

	private

		def generate_authentication_token
			loop do
				token = Devise.friendly_token
				break token unless AdminUser.where(authentication_token: token).first
			end
		end
end
