class BrandProduct < ActiveRecord::Base
	belongs_to :brand
	has_many :products
	has_attached_file :img_v2_url, styles: { thumb: "100x100>" }
	validates_attachment_content_type :img_v2_url, content_type: /\Aimage\/.*\Z/
end
