class NetPromotingScore < ActiveRecord::Base
	validates :numeric_score, :inclusion => {:in => 1..5}
	belongs_to :order

	def to_s
		"#{numeric_score} puntos, creado el #{created_at.strftime("%d de %b")}"
	end
end
