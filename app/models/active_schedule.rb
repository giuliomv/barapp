class ActiveSchedule < ActiveRecord::Base
	attr_accessor :initial_time
	attr_accessor :end_time
  belongs_to :company
	DAYS_OF_WEEK = [["Lunes", 2], ["Martes", 3], ["Miércoles", 4], ["Jueves", 5], ["Viernes", 6], ["Sábado", 7], ["Domingo", 1]]

	before_save :save_minutes_seconds

  protected
  def save_minutes_seconds
  	self.initial_hour = initial_time.split(":")[0]
  	self.initial_minutes = initial_time.split(":")[1]
  	self.end_hour = end_time.split(":")[0]
  	self.end_minutes = end_time.split(":")[1]
  end
end
