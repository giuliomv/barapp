class Company < ActiveRecord::Base
	acts_as_mappable 	:default_units => :kms,
						:lat_column_name => :latitude,
						:lng_column_name => :longitude
	has_many :admin_users
	has_many :products
	has_many :combos
	has_many :active_schedules
	has_many :schedule_texts
	has_many :orders
	has_many :delivery_tiers, -> { order(:delivery_value => :asc) }
	has_many :company_zones
	has_many :company_banner_ads

	attr_accessor :delivery_fee

	accepts_nested_attributes_for :products, :combos, :active_schedules, :schedule_texts, :orders

	def location
		if self.latitude and self.longitude
			[self.latitude, self.longitude]
		else
			nil
		end
	end

	def banner_entities
		banners = []
		banners += self.combos.where(:deleted => false) + self.company_banner_ads
		banners.sort_by!{|b_e| b_e[:order_key]}
	end

	def current_zone
		company_zones.where(:active=>true).first
	end

	def self.gmaps_companies_hash(parameters)
		companies = all
		company_markers = Gmaps4rails.build_markers(companies) do |company, marker|
		  marker.lat company.latitude
		  marker.lng company.longitude
		  marker.title "#{company.id}"
		  marker.infowindow "#{company.created_at}"
		  marker.picture ({
		                  :url    => ActionController::Base.helpers.asset_path("pin.png"),
		                  :width  => "14",
		                  :height => "38",
		                  :scaledWidth => "7", # Scaled width is half of the retina resolution; optional
		                  :scaledHeight => "19", # Scaled width is half of the retina resolution; optional
		                 })
		end
		middle_points = []
		companies_temp = companies.to_a
		while companies_temp.size > 1
			initial_company = companies_temp.first
			(1..companies_temp.count-1).each do |n|
				middle_points << initial_company.midpoint_to(companies_temp[n])
			end
			companies_temp.shift
		end

		company_middle_points_markers = Gmaps4rails.build_markers(middle_points) do |mid_point, marker|
		  marker.lat mid_point.latitude
		  marker.lng mid_point.longitude
		  marker.picture ({
		                  :url    => ActionController::Base.helpers.asset_path("jewel_pin.png"),
		                  :width  => "20",
		                  :height => "18",
		                  :scaledWidth => "10", # Scaled width is half of the retina resolution; optional
		                  :scaledHeight => "9", # Scaled width is half of the retina resolution; optional
		                 })
		end
		@hash = company_markers + company_middle_points_markers
		@hash
	end

	def delivery_by_distance(order_location)
		distance = self.distance_to(order_location, :units => :kms)
		tiers = DeliveryTier.where(company_id: self.id).order(distance_limit: :asc)
		tiers.each do |t|
			if distance < t.distance_limit
				return t.delivery_value
			end
		end
		return self.highest_delivery_fee
	end
end
