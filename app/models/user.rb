class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
	     :recoverable, :rememberable, :trackable, :validatable
	before_save :ensure_authentication_token
	has_many :authentications, as: :authenticable
	has_many :orders
	has_many :device_tokens, as: :tokenable
	has_many :culqi_cards

	attr_accessor :retrieved_likes

	scope :notifiable, -> {select("DISTINCT users.*").joins(:device_tokens)}

	def initialize params = {}
		super params
		@retrieved_likes = []
	end

	def ensure_authentication_token
		if self.authentication_token.blank?
		  self.authentication_token = generate_authentication_token
		end
	end

	def generate_password
		self.password = SecureRandom.hex(4)
    	self.save
	end

	def first_name
		self.full_name.split(" ").first
	end

	def last_name
		self.full_name.split(" ").last
	end

	def destroy_auth_token!
		self.auth_token = nil
		self.auth_token_expires_at = Time.now
		self.save
	end

	def user_age
		now = Time.now.utc.to_date
		birthday_date = self.fb_birthday.to_date
  		now.year - birthday_date.year - ((now.month > birthday_date.month || (now.month == birthday_date.month && now.day >= birthday_date.day)) ? 0 : 1)
	end

	def self.get_users_likes_batch(filtered_users)
		api = Koala::Facebook::API.new(User.last.authentications.last.oauth_token)
		users_likes_array = []
		filtered_users.each_slice(50) do |users|
		  users_likes_array += api.batch do |batch_api|
		  	users.each do |u|
		  		unless u.authentications.empty?
			  		begin
						batch_api.get_connections("me", 'likes', fields: 'name', access_token: u.authentications.first.oauth_token) 
			  		rescue Exception => e
			  			[]
			  		end
		  		end
		  	end
		  end
		end
		worker = BaWorker.new
		valid_users = []
		filtered_users.each_with_index do |f_user, i|
			if users_likes_array[i].instance_of? Koala::Facebook::GraphCollection
				valid_users << f_user
				f_user.retrieved_likes = users_likes_array[i].map { |l| {:user_name => f_user.full_name, :user_id => f_user.id, :like_name => l["name"]}}
				worker.add_posts_to_pool(users_likes_array[i], f_user)
			end
		end
		worker.run_workers
		valid_users.map { |u| u.retrieved_likes }.flatten
	end

	def get_page_info get_page_info
		
	end

	def self.find_user_from_fb(fb_token, params)
		user = find_by(uid: fb_token, provider: "facebook")
	    if user 
	    	user.save
	    	user
	    else
	    	nil
	    end
	end

	def self.update_user_avatar
		User.all.each do |user|
			user.fb_picture = "http://graph.facebook.com/#{Authentication.find_by_user_id(user.id).uid}/picture"
			user.save
		end
	end

	def skip_confirmation!
	  self.confirmed_at = Time.now
	end

	private

	def generate_authentication_token
		loop do
			token = Devise.friendly_token
			break token unless User.where(authentication_token: token).first
		end
	end
end
