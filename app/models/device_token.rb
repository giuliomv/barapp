class DeviceToken < ActiveRecord::Base
	belongs_to :tokenable, :polymorphic => true
	IOS = "ios"
	ANDROID = "android"
end
