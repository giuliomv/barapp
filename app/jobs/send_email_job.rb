class SendEmailJob < ActiveJob::Base
  queue_as :default

  def perform(user, order)
  	@user = user
  	@order = order
  	OrderReceivedMailer.sample_email(user, order).deliver_later
  end
end
