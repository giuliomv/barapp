YOUR_APP = {
    combos:{}
}

jQuery(document).ready(function($) {
       // get the page action
    var action,model, b = $("body");
    if (b.hasClass("edit")) {
        action = "edit";
    } else if(b.hasClass("view")){
        action = "view";
    } else if(b.hasClass("index")){
        action = "index"
    } else if(b.hasClass("new")){
        action = "new"
    }

        // run the code specific to a model and an action
    for (var m in YOUR_APP) {
        if (b.hasClass("admin_"+m) && ((action === "new") || (action === "edit")) ) {

            $("#get_total_combo_button").click(function () {
                calculateTotal();
            });

            $("#new_combo").bind("DOMSubtreeModified", function() {
                var subtotals = $("[id^=combo_combo_details_attributes_][id$=_subtotal]");
                subtotals.each(function () {
                    if (!jQuery._data($(this)[0], "events") || !jQuery._data($(this)[0], "events")["change"]) {
                        (subtotals).change(function() {
                            calculateTotal();
                        });
                    };
                })

                var products = $("[id^=combo_combo_details_attributes_][id$=_product_id]");
                products.each(function () {
                    if (!jQuery._data($(this)[0], "events") || !jQuery._data($(this)[0], "events")["change"]) {
                        (products).change(function() {
                            getPrice(this.value, this.id);
                        });
                    }
                })

                var categories = $("[id^=combo_combo_details_attributes_][id$=_category]");
                categories.each(function () {
                    if (!jQuery._data($(this)[0], "events") || !jQuery._data($(this)[0], "events")["change"]) {
                        (categories).change(function() {
                            getBrandsByCategoryId(this.value, this.id);
                        });
                    }
                })

                var brands = $("[id^=combo_combo_details_attributes_][id$=_brand]");
                brands.each(function () {
                    if (!jQuery._data($(this)[0], "events") || !jQuery._data($(this)[0], "events")["change"]) {
                        (brands).change(function() {
                            getProductsByBrandId(this.value, this.id);
                        });
                    }
                })

                var quantities = $("[id^=combo_combo_details_attributes_][id$=_qty]");
                quantities.each(function () {
                    if (!jQuery._data($(this)[0], "events") || !jQuery._data($(this)[0], "events")["change"]) {
                        (quantities).change(function() {
                            calculateSubtotal(this.id);
                        });
                    }
                })
            });

            var getBrandsByCategoryId = function (_category_id, category) {
                category = "" + category
                id = category.slice("combo_combo_details_attributes_".length, category.lastIndexOf("_category"));
                $.ajax({
                    url: "/admin/brands/get_brands_by_category_id",
                    dataType: 'json',
                    data: {
                      category_id: _category_id
                    },
                    success: function(data) {
                        $("#combo_combo_details_attributes_" + id + "_brand").empty().append('Seleccina una marca');
                        $.each(data, function () {
                            $("#combo_combo_details_attributes_" + id + "_brand").append($("<option />").val(this.id).text(this.name));
                        })
                        $("#combo_combo_details_attributes_" + id + "_brand").prepend("<option value='' selected='selected'></option>");
                    }
                });
            }

            var getProductsByBrandId = function (_brand_id, brand) {
                brand = "" + brand
                id = brand.slice("combo_combo_details_attributes_".length, brand.lastIndexOf("_brand"));
                _company_id = $("#combo_company_id").val();
                $.ajax({
                    url: "/admin/companies/:product_company_id/products/get_products_by_brand_id",
                    dataType: 'json',
                    data: {
                      brand_id: _brand_id,
                      requested_company_id: _company_id
                    },
                    success: function(data) {
                        $("#combo_combo_details_attributes_" + id + "_product_id").empty().append('Seleccina una marca');
                        $.each(data, function () {
                            $("#combo_combo_details_attributes_" + id + "_product_id").append($("<option />").val(this.id).text(this.name));
                        })
                        $("#combo_combo_details_attributes_" + id + "_product_id").prepend("<option value='' selected='selected'></option>");
                    }
                });
            }

            var getPrice = function (_product_id, product) {
                product = "" + product
                id = product.slice("combo_combo_details_attributes_".length, product.lastIndexOf("_product_id"));
                $.ajax({
                    url: "/admin/companies/:product_company_id/products/get_product_by_id",
                    dataType: 'json',
                    data: {
                      product_id: _product_id
                    },
                    success: function(data) {
                        $("#combo_combo_details_attributes_" + id + "_product_price").val(data.unitPrice);
                    }
                });
            }

            var calculateSubtotal = function (qtySelector) {
                qtySelector = "" + qtySelector
                var id = qtySelector.slice("combo_combo_details_attributes_".length, qtySelector.lastIndexOf("_qty"));
                var qty = $("#combo_combo_details_attributes_" + id + "_qty");
                var unitPrice = $("#combo_combo_details_attributes_" + id + "_product_price");
                console.log(qty.val())
                console.log(unitPrice.val())
                $("#combo_combo_details_attributes_" + id + "_subtotal").val(parseInt(qty.val())*parseFloat(unitPrice.val()));
            }

            var calculateTotal = function () {
                var total = 0;
                $("[id^=combo_combo_details_attributes_][id$=_subtotal]").each(function() {
                    total = total + parseInt($(this).val());
                });
                $("#combo_price").val(total);
            }

        }
    }
});