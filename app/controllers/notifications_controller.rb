class NotificationsController < ApplicationController
  def index
  	flash[:notice] = nil
  	@orders = Order.all.order(created_at: :desc).limit(10)
  	if request.xhr?
		flash[:notice] = "Órden nueva!"
  		render partial: "orders_list"
  	else
  	end
  end
end
