class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  include Authenticable

  def current_ability
    @current_ability ||= UserAbility.new(current_user)
  end

  def self.get_local_date_from_string(string_date, days_added)
    timed_date = string_date.to_date
    zoned_date = Time.zone.local(timed_date.year, timed_date.month, timed_date.day + days_added)
  end

end
