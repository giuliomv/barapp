class Api::V2::MeController < ApplicationController 

  def fidelity_points
    if request.headers['Authorization']
      authenticate_with_http_token do |token, options|
        u = User.find_by(authentication_token: token)
        if u
          render :status => 200, :json => { :success => true,
                      :info => "Logged in user total fidelity points",
                      :fidelity_points => u.total_fidelity_points,
                      :avatar_url => u.fb_picture,
                      :user_email => u.email }
        else
          render :status => 401,
                 :json => { :success => false,
                            :info => "Invalid access token",
                            :data => {} }
        end
      end
    else
      render :status => 401,
                 :json => { :success => false,
                            :info => "Auth token not included",
                            :data => {} }
    end
  end

  def credit_cards
    if request.headers['Authorization']
      authenticate_with_http_token do |token, options|
        u = User.find_by(authentication_token: token)
        if u
          render :status => 200, :json => { :success => true,
                      :info => "Logged in user cards",
                      :cards => u.culqi_cards.where(deleted: false)}
        else
          render :status => 401,
                 :json => { :success => false,
                            :info => "Invalid access token",
                            :data => {} }
        end
      end
    else
      render :status => 401,
                 :json => { :success => false,
                            :info => "Auth token not included",
                            :data => {} }
    end
  end

  def my_orders
    if request.headers['Authorization']
      authenticate_with_http_token do |token, options|
        u = User.find_by(authentication_token: token)
        if u
          render :status => 200, :json => { :success => true,
                      :info => "Logged in user orders",
                      :orders => u.orders.order('id desc')[0..14].as_json({
                        :include => {
                          :combo_orders => {
                            :include => {
                              :combo => {
                                :only =>[
                                  :name, :price, :description
                                  ],
                                :methods => [
                                  :img_shopping_cart_url
                                ]
                              }
                            }
                          }, 
                          :order_details => {
                            :include => {
                              :product => {
                                :only => [
                                  :unitPrice
                                ],
                                :methods =>[
                                  :product_img_v2_url, :name, :description
                                ]
                              }
                            }
                          }
                        }
                      })}
        else
            render json: { error: 'Bad Token'}, status: 401
        end
      end
    else
      render json: { error: 'Missing token'}, status: 401
    end
  end

end
