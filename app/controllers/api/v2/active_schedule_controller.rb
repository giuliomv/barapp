class Api::V2::ActiveScheduleController < ApplicationController
  def index
  	@schedules = ActiveSchedule.where(:company_id=>params[:company_id])
  	current_text = ScheduleText.where(:company_id=>params[:company_id]).first
	render json: {"schedules" => @schedules, "schedule_text" => current_text}
  end
end
