class Api::V2::RegistrationsController < ApplicationController
	def create
		user = User.new(user_params)
		user.generate_password if params[:access_token]
	    if user.save
	    	if params[:access_token]
	    		graph = Koala::Facebook::API.new(params[:access_token])
            	data = graph.get_object(:me, { fields: [:id, :name, :age_range, :link, :gender, :picture, :birthday]})
	    		account = Authentication.new(:provider => "facebook", :uid => data["id"], :oauth_token => params[:access_token])
	    		user.fb_age_range = data["age_range"]["min"] if (data["age_range"]!=nil)
	    		user.fb_link = data["link"]
	    		user.fb_gender = data["gender"]
	    		user.fb_picture = "http://graph.facebook.com/#{data["id"]}/picture"
	    		user.save
	    		account.oauth_token_expires_at = DateTime.strptime(params[:token_expires_at], '%Y-%m-%d %H:%M:%S %z')
	    		account.authenticable = user
	    		account.save
	    	end
	     	render :status => 200,
                 :json => { :success => true,
                            :info => "Signed up",
                            :data => { :user => user, :new_user => true}}
	    else
	      warden.custom_failure!
	      render :json=> user.errors, :status=>422
	    end
	end

	private

		def user_params
	      params[:user].permit(:full_name, :email, :phone_number, :fb_birthday, :document_number, :accepts_ads)
	    end
end
