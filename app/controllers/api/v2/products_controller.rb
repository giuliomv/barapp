class Api::V2::ProductsController < ApplicationController
	def index
		company_brand_products = Product.joins(brand_product: {brand: :category}).where(brands: {category_id:params[:category_id]},company_id: params[:company_id])
		render json: company_brand_products, :only => [:description, :updated_at, :deleted, :flg_stock, :id, :name, :unitPrice, :order_key], :methods => [:name, :description, :product_img_v2_url, :brand_id]
	end
end
