class Api::V2::CompaniesController < ApplicationController
	# def closest_company
	# 	if ((params[:lat].to_f - 0.0).abs <= 0.01) or ((params[:lng].to_f - 0.0).abs <= 0.01)
	# 		company = Company.find(10)
	# 	else
	# 		@companies = Company.where(:deleted => false)
	# 		company = @companies.sort_by{|s| s.distance_to([params[:lat].to_f, params[:lng].to_f])}.first
	# 	end
	# 	delivery_fee = company.delivery_by_distance([params[:lat].to_f, params[:lng].to_f])
	# 	render :json => company.as_json(:include => [:delivery_tiers =>{:only => [:delivery_value]}]).merge(:delivery_fee => delivery_fee)
	# end
	def closest_company
		is_valid_company = false
		if ((params[:lat].to_f - 0.0).abs <= 0.01) or ((params[:lng].to_f - 0.0).abs <= 0.01)
			is_valid_company = true
			company = Company.find(13)
		else
			@companies = Company.where(:deleted => false)
			sorted_companies = @companies.sort_by{|s| s.distance_to([params[:lat].to_f, params[:lng].to_f])}
			reachable_company = sorted_companies.find{|comp| comp.current_zone.point_in_polygon?(params[:lat].to_f, params[:lng].to_f)}
			if reachable_company.nil?
				company = sorted_companies.first
			else
				is_valid_company = true
				company = reachable_company
			end
		end
		delivery_fee = company.delivery_by_distance([params[:lat].to_f, params[:lng].to_f])
		render :json => company.as_json(:include => [:delivery_tiers =>{:only => [:delivery_value]}]).merge(:delivery_fee => delivery_fee, :is_valid_company => is_valid_company)
	end

	def company_banners
		ad_banners = Company.find(params[:company_id]).company_banner_ads
		combo_banners = Company.find(params[:company_id]).combos.where(:deleted => false)

		render :json =>{:ad_banners => ad_banners.as_json(:only =>[:id, :order_key], :methods=>[:name, :img_url, :banner_type]),
						:combo_banners => combo_banners.as_json(:only => [:created_at, :deleted, :flg_stock, :id, :name, :price, :updated_at, :order_key, :description], :methods => [:img_v2_url, :img_shopping_cart_url, :banner_type], :include => [:combo_details])}
	end

	def find_items
		@products = Product.where(company_id: params[:company_id], deleted: false).search(params[:query])
		@combos = Combo.where(company_id: params[:company_id], deleted: false).search(params[:query])
		render json: {"products" => @products.as_json(:only =>[:description, :updated_at, :deleted, :flg_stock, :id, :name, :unitPrice, :order_key], :methods => [:name, :description, :product_img_v2_url, :brand_id, :category_id]), "combos" => @combos.as_json(:only =>[:created_at, :deleted, :flg_stock, :id, :name, :price, :updated_at, :order_key, :description], :methods => [:img_v2_url, :img_shopping_cart_url])}
	end

end