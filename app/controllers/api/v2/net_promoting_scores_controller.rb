class Api::V2::NetPromotingScoresController < ApplicationController
	def create
		nps = NetPromotingScore.new
		nps.assign_attributes(nps_params)
		nps.save
		if nps.save
			render :status => 200,
                 :json => { :success => true,
                            :info => "Net Promoting Score submitted",
                            :data => { :nps => nps}}
		else
			render :json=> nps.errors, :status=>422
		end

	end

	private
	    def nps_params
	      params.require(:nps).permit(:numeric_score, :order_id, :comment)
	    end
end
