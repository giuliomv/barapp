class Api::V2::DeviceTokensController < ApplicationController
	def create
		if request.headers['Authorization']
			authenticate_with_http_token do |token, options|
				tokenable_type = "User"
				if params["role"] != nil
					tokenable_type = "AdminUser"
					u = AdminUser.find_by(authentication_token: token)
				else
					u = User.find_by(authentication_token: token)
				end
				if u
					platform_tokens = DeviceToken.where(tokenable_id: u.id, device_platform: device_token_params["device_platform"], tokenable_type: tokenable_type)
					if platform_tokens.empty?
						d_token = DeviceToken.new
						d_token.assign_attributes(device_token_params)	
						d_token.tokenable = u
					else
						d_token = platform_tokens.first
						d_token.assign_attributes(device_token_params)
					end
					if d_token.save
			        	render :status => 200, :json => { :success => true,
			                        :info => "Device token stored",
			                        :d_token => d_token }
					else
						render :status => :unprocessable_entity,
			               :json => { :success => false,
			                          :info => d_token.errors,
			                          :data => {} }
					end
				else
					render :status => 401,
                 			:json => { :success => false,
                            :info => "Invalid access token",
                            :data => {} }
				end
			end
		else
 			render :status => 401,
                 :json => { :success => false,
                            :info => "Auth token not included",
                            :data => {} }
		end
	end

	private

	    def device_token_params
	      params.require(:device_token).permit(:token_string, :device_platform)
	    end
end
