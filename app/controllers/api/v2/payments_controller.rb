class Api::V2::PaymentsController < ApplicationController
	def register_card
		if request.headers['Authorization']
			authenticate_with_http_token do |token, options|
		    	u = User.find_by(authentication_token: token)
				if u
					card_json = CulqiCard.create_remote_card(u, params["address"], params["address_city"], params["culqi_token"])
					if card_json["object"] != "error"
						card_obj = CulqiCard.new
						card_obj.id_card = card_json["id"]
						card_obj.last_four = card_json["source"]["last_four"]
						card_obj.card_brand = card_json["source"]["iin"]["card_brand"]
						card_obj.user = u
						if card_obj.save
							render :status => 200,
				             :json => { :success => true,
				                        :info => "Card created",
				                        :card => card_obj
				                      }      
						else
							render :status => :unprocessable_entity,
				               :json => { :success => false,
				                          :info => card_obj.errors,
				                          :data => {} }
						end
					else
						render :status => 200,
				             :json => { :success => false,
				                        :info => "Card not created",
				                        :card => card_json["merchant_message"]
				                      }
					end
				else
		    		render :status => 401,
                 			:json => { :success => false,
                            :info => "Invalid credentials",
                            :data => {} }
				end
		    end
		else
			render :status => 401,
                 :json => { :success => false,
                            :info => "Auth token not included",
                            :data => {} }
		end
	end

	def delete_card
		if request.headers['Authorization']
			authenticate_with_http_token do |token, options|
		    	u = User.find_by(authentication_token: token)
				if u
					card = CulqiCard.find(params[:card_id])
					if card and (u.id == card.user_id)
						card.deleted = true
						if card.save
							render 	:status => 200,
	           						:json => { :success => true,
	                      				:info => "Card deleted",
	                      				:data => "Card deleted successfully"
	                      			}
						else
							render :status => :unprocessable_entity,
               						:json => { :success => false,
                          			:info => card.errors,
                          			:data => {} }
						end
					else
						render :status => 401,
                 			:json => { :success => false,
                            :info => "Invalid credentials",
                            :data => {} }
					end
				else
		    		render :status => 401,
                 			:json => { :success => false,
                            :info => "Invalid credentials",
                            :data => {} }
				end
		    end
		else
			render :status => 401,
                 :json => { :success => false,
                            :info => "Improcesable entities",
                            :data => {} }
		end
	end

end
