class Api::V2::SessionsController < ApplicationController
    skip_before_action :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }
    respond_to :json
	def create
    session = params["session"]
    if session
      fb_auth = session["fb_auth"]
      if fb_auth
        #Regular User Login
        begin
          graph = Koala::Facebook::API.new(fb_auth)
          data = graph.get_object(:me, { fields: [:id, :name, :picture, :email, :birthday, :first_name]})
          account = Authentication.find_by(:provider => "facebook", :uid => data["id"])
          if account.nil?
            render :status => 200,
               :json => { :success => true,
                          :info => "New user",
                          :data => { :basic_info => data, :new_user => true } }
          else
            account.oauth_token = fb_auth
            account.oauth_token_expires_at = Time.now + account[:oauth_token_expires_at].to_i.seconds
            user = account.authenticable
            user.save
            render :status => 200,
               :json => { :success => true,
                          :info => "Logged in",
                          :data => { :user => user, :new_user => false} }
          end
        rescue
          render :status => 401,
               :json => { :success => false,
                          :info => "Invalid access token",
                          :data => {} }
        end
      else
        #Admin User Login
        username = session["ba_user"]
        password = session["ba_password"]
        admin_user = username.present? && AdminUser.find_by(email: username)
        if admin_user.valid_password? password
          account = Authentication.find_by(:provider => "regular", :authenticable_type => "AdminUser", :authenticable_id => admin_user.id)
          if account.nil?
            account = Authentication.new(:provider => "regular")
            account.authenticable = admin_user
            account.save
            admin_user.ensure_authentication_token
            admin_user.save
          end
          render :status => 200,
               :json => { :success => true,
                          :info => "Logged in",
                          :data => { :admin_user => admin_user}}
        else
          render :status => 401,
                 :json => { :success => false,
                            :info => "Invalid credentials",
                            :data => {} }
        end
      end
    else
      render :status => 401,
                 :json => { :success => false,
                            :info => "Improcesable entities",
                            :data => {} }
    end
  end

  def destroy
    user = User.find(params[:authentication_token])
    user.destroy_auth_token!
    render :status => 200,
           :json => { :success => true,
                      :info => "Logged out",
                      :data => {} }
  end
  
  private
      
        def user_params
          params[:user].permit(:first_name, :last_name, :email, :password, :country_id, :location, :home_zipcode, :phone, :work_zipcode, :gender)
        end

        def fb_user_params
          params[:user].permit(:first_name, :last_name, :email, :location, :gender, :remote_avatar_url, :remote_cover_url, :country_id, :home_zipcode, :phone, :work_zipcode)
        end
end
