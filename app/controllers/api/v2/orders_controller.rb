#encoding: utf-8
class Api::V2::OrdersController < ApplicationController
	def index
		if request.headers['Authorization']
			authenticate_with_http_token do |token, options|
		    	u = AdminUser.find_by(authentication_token: token)
				if u
					if u.superadmin
						render json: Order.order('id desc').limit(15), :methods => [:products_string, :combos_string, :company_name, :current_state]
					else
						render json: u.company.orders.order('id desc')[0..14], :methods => [:products_string, :combos_string, :company_name, :current_state]
					end
				else
		    		render json: { error: 'Bad Token'}, status: 401
				end
		    end
		else
			render json: { error: 'Missing token'}, status: 401
		end
	end

	def change_state
		if request.headers['Authorization']
			authenticate_with_http_token do |token, options|
		    	u = AdminUser.find_by(authentication_token: token)
				if u
					order = Order.find(params[:id])
					message = ""
					if order and (u.superadmin or (u.company_admin and u.company_id == order.company_id))
						accept_message = ""
						case params[:state_id].to_i
							when 2
								order.in_transit_order
							when 3
								accept_message = order.accept_order
							when 4
								order.reject_order
							when 5
								order.finalize_order
						end
						if order.save
							if params[:state_id].to_i == 3
								if accept_message == "venta_exitosa"
									if order.user and !order.user.device_tokens.empty?
										push_n = PushNotification.new
										push_n.notification_title = "Bar App"
										push_n.notification_message = "Tu pedido ha sido aceptado y en breve será despachado"
										if push_n.save!
										  data = {order_update_push: true, order_id: order.id, state_id: 3}
										  dt = DeviceToken.where(tokenable_type: "User", tokenable_id: order.user_id, device_platform: order.platform.downcase).first
										  if order.platform.downcase == DeviceToken::IOS
										    push_n.send_ios_notification(dt, data)
										  else
										    push_n.send_android_notification([dt], data, "android_app")
										  end
										end
									end
									order.update_attribute(:state_id, 3)
									render 	:status => 200,
	               						:json => { :success => true,
	                          				:info => accept_message,
	                          				:data => order.as_json(:methods => [:products_string, :combos_string, :company_name, :current_state])
	                          			}
								else
									payment_info = order.payments.last
									render 	:status => 200,
	               						:json => { :success => false,
	                          				:info => payment_info.message_user,
	                          				:data => order.as_json(:methods => [:products_string, :combos_string, :company_name, :current_state])
	                          			}
								end
							else
								render 	:status => 200,
	               						:json => { :success => true,
	                          				:info => "Order Changed",
	                          				:data => order.as_json(:methods => [:products_string, :combos_string, :company_name, :current_state])
	                          			}
								
							end

						else
							render :status => :unprocessable_entity,
               						:json => { :success => false,
                          			:info => order.errors,
                          			:data => {} }
						end
					else
						render :status => 401,
                 			:json => { :success => false,
                            :info => "Invalid credentials",
                            :data => {} }
					end
				else
		    		render :status => 401,
                 			:json => { :success => false,
                            :info => "Invalid credentials",
                            :data => {} }
				end
		    end
		else
			render :status => 401,
                 :json => { :success => false,
                            :info => "Improcesable entities",
                            :data => {} }
		end
	end

	def create
		@order = Order.new
		@order.assign_attributes(order_params)
		if request.headers['Authorization']
			authenticate_with_http_token do |token, options|
		    	u = User.find_by(authentication_token: token)
				@order.user = u
		    end
		end

		if @order.save
			if params[:coupon]
				coupon = Coupon.find(params[:coupon][:id])
				@order.attach_coupon(coupon)
				coupon.redeem
			end
			if params[:delivery_fee]
				@order.include_delivery(params[:delivery_fee])
			end
			if params[:order][:payment_method] == "online"
				@order.card_id_selected = params[:payment_data][:id_card].to_i
				@order.save
			end

			@order.calculate_fidelity_points
			@order.send_order_received_mail

			Thread.new do
				begin
				    Pusher.trigger('bar_app_order_created_channel', 'order_created', {
					  message: "#{@order.id}", company: "#{@order.company_id}"
					})
			    rescue Pusher::Error => e
			    	Rails.logger.error "Pusher error: #{e.message}"
			    end
			end

			@order.send_notification_to_admin
			render :status => 200,
             :json => { :success => true,
                        :info => "Order created",
                        :order => @order
                      }      
        else
        	render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => @order.errors,
                          :data => {} }
		end
	end

	private

	    def order_params
	      params.require(:order).permit(:contact_name, :delivery_address, :address_reference, :contact_phone, :total, :payment_method, :state_id, :latitude, :longitude, :platform, :company_id, :cash_amount, :order_details_attributes => [:product_id, :qty, :subtotal], :combo_orders_attributes => [:combo_id, :qty, :subtotal])
	    end
end
