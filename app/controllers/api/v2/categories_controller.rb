class Api::V2::CategoriesController < ApplicationController
	def index
		company_id = params[:company_id].blank? ? 1 : params[:company_id]
		@categories = Category.with_products_available(company_id)
		render json: @categories, :only => [:name, :description, :updated_at, :deleted, :img_code, :id, :order_key, :alcoholic], :methods => [:category_banner_img_url], :include => [:visual_brands => {:only =>[:id, :name, :category_id], :methods => [:brand_banner_img_url]}]
	end
end
