class Api::V2::CouponsController < ApplicationController
	def valid_coupon
		valid = Coupon.is_valid?(params[:input_code])
		render json: { coupon_valid: valid, coupon: (valid ? Coupon.find_by_code(params[:input_code]) : "")}, status: 200
	end
end
