class Api::V2::BrandProductsController < ApplicationController
	def index
		@brand_products = BrandProduct.where(:is_fidelity => true)
		render json: @brand_products, :only => [:name, :description, :fidelity_points, :id, :created_at], :methods => [:img_v2_url]
	end
end
