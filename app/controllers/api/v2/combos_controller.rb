class Api::V2::CombosController < ApplicationController
	def index
		@combos = Combo.where(:company_id=>params[:company_id], :deleted => false)
		render json: @combos, :only => [:created_at, :deleted, :flg_stock, :id, :name, :price, :updated_at, :order_key, :description], :methods => [:img_v2_url, :img_shopping_cart_url], :include => [:combo_details]
	end
end
