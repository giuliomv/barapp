class Api::V2::BrandsController < ApplicationController
	def index
		@brands = Brand.find_by_category_id(params[:category_id])
	    render json: @brands
	end
end
