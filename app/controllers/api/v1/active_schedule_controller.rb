class Api::V1::ActiveScheduleController < ApplicationController
  def index
  	@schedules = ActiveSchedule.where(:company_id=>1)
  	current_text = ScheduleText.where(:company_id=>1).first
	render json: {"schedules" => @schedules, "schedule_text" => current_text}
  end
end
