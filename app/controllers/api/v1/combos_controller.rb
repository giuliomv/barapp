class Api::V1::CombosController < ApplicationController
	def index
		@combos = Combo.where(:company_id=>1)
		render json: @combos, :only => [:created_at, :deleted, :flg_stock, :id, :name, :price, :updated_at], :methods => [:img_url], :include => [:combo_details]
	end
end
