class Api::V1::ProductsController < ApplicationController
	def index
		@products = Product.joins(:brand => :category).where(:brands=>{:category_id=>params[:category_id]},:company_id=>1)
		render json: @products, :only => [:brand_id, :updated_at, :deleted, :flg_stock, :id, :name, :unitPrice], :methods => [:product_img_url]
	end
end
