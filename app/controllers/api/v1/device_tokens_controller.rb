class Api::V1::DeviceTokensController < ApplicationController
	def create
		d_token = DeviceToken.new
		d_token.assign_attributes(device_token_params)
		if !DeviceToken.exists?(:token_string => device_token_params[:token_string], :device_platform => device_token_params[:device_platform])
			if d_token.save
	        	render :status => 200,
	             :json => { :success => true,
	                        :info => "Device token stored",
	                        :d_token => d_token }
			else
				render :status => :unprocessable_entity,
	               :json => { :success => false,
	                          :info => d_token.errors,
	                          :data => {} }
			end
		else
			render :status => 200,
	             :json => { :success => true,
	                        :info => "Device already registered",
	                        :d_token => d_token }
		end
	end

	private

	    def device_token_params
	      params.require(:device_token).permit(:token_string, :device_platform)
	    end
end
