#encoding: utf-8
class Api::V1::OrdersController < ApplicationController
	before_action :authenticate_with_token!, only: [:index]

	def index
		@orders = current_user.orders
		render json: @orders, 
		:include => {:order_details => {:include => :product}, :combo_orders => {:include => :combo}}
	end

	def create
		@order = Order.new
		@order.assign_attributes(order_params)
		@order.company_id = 1
		if @order.save
			Pusher.trigger('bar_app_order_created_channel', 'order_created', {
			  message: "#{@order.id}", company: "#{@order.company_id}"
			})
			render :status => 200,
             :json => { :success => true,
                        :info => "Order created",
                        :order => @order
                      }      
        else
        	render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => @order.errors,
                          :data => {} }
		end
	end

	private

	    def order_params
	      params.require(:order).permit(:contact_name, :delivery_address, :address_reference, :contact_phone, :total, :payment_method, :state_id, :latitude, :longitude, :platform, :order_details_attributes => [:product_id, :qty, :subtotal], :combo_orders_attributes => [:combo_id, :qty, :subtotal])
	    end
end
