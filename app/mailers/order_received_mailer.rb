class OrderReceivedMailer < ActionMailer::Base
  default from: "Equipo BarApp <barappdelivery@outlook.com>"


  def sample_email(user, order)
    @user = user
    @order = order
    mail(to: @user.email, subject: "[Bar App] #{@user.first_name}, este es el resumen tu pedido del #{l @order.created_at, :format => "%A %d", :locale => 'es'}")
  end
end