class AddImgCodeToCategory < ActiveRecord::Migration[5.1]
  def change
  	add_column :categories, :img_code, :string, default: ""
  end
end
