class ChangeCardIdSelectedToInteger < ActiveRecord::Migration[5.1]
  def change
  	change_column :orders, :card_id_selected, :integer
  end
end
