class AddAlcoholicToCategory < ActiveRecord::Migration[5.1]
  def change
  	add_column :categories, :alcoholic, :boolean, default: true
  end
end
