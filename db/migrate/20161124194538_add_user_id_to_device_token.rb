class AddUserIdToDeviceToken < ActiveRecord::Migration[5.1]
  def change
  	add_reference :device_tokens, :user, index: true
  end
end
