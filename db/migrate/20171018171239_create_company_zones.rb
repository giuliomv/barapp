class CreateCompanyZones < ActiveRecord::Migration[5.1]
  def change
    create_table :company_zones do |t|
    	t.text :area_limits
    	t.references :company, index: true
    	t.boolean :active
    	t.timestamps null: false
    end
  end
end
