class AddFidelizationToBrandProduct < ActiveRecord::Migration[5.1]
  def change
  	add_column :brand_products, :is_fidelity, :boolean, default: false
  	add_column :brand_products, :fidelity_points, :integer, default: 0
  end
end
