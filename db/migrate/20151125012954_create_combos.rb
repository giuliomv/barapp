class CreateCombos < ActiveRecord::Migration[5.1]
  def change
    create_table :combos do |t|
      t.string :name
      t.text :description
      t.decimal :price

      t.timestamps
    end
  end
end
