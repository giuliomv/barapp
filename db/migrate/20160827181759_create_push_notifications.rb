class CreatePushNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :push_notifications do |t|
      t.string :notification_title
      t.string :notification_message
      t.boolean :to_android
      t.boolean :to_ios

      t.timestamps
    end
  end
end
