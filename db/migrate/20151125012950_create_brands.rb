class CreateBrands < ActiveRecord::Migration[5.1]
  def change
    create_table :brands do |t|
      t.string :name
      t.references :category, index: true
      t.text :description
      t.boolean :deleted
      t.boolean :flg_stock

      t.timestamps
    end
  end
end
