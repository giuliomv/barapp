class AddAttachmentImgUrlToCombos < ActiveRecord::Migration[5.1]
  def self.up
    change_table :combos do |t|
      t.attachment :img_url
    end
  end

  def self.down
    remove_attachment :combos, :img_url
  end
end
