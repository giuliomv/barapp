class FixStringsDataTypes < ActiveRecord::Migration[5.1]
  def change
  	change_column :users, :fb_picture, :text
  	change_column :authentications, :oauth_token, :text
  end
end
