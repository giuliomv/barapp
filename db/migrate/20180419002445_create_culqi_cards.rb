class CreateCulqiCards < ActiveRecord::Migration[5.1]
  def change
    create_table :culqi_cards do |t|
    	t.string :id_card
    	t.string :id_customer
    	t.string :card_brand
    	t.string :last_four
    	t.references :user, index: true
    	t.timestamps null: false
    end
  end
end
