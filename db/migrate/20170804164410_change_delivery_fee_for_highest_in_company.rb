class ChangeDeliveryFeeForHighestInCompany < ActiveRecord::Migration[5.1]
  def change
  	rename_column :companies, :delivery_fee, :highest_delivery_fee
  end
end
