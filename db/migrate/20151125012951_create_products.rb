class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.references :brand, index: true
      t.decimal :unitPrice
      t.boolean :flg_stock
      t.integer :stock_qty
      t.boolean :deleted

      t.timestamps
    end
  end
end
