class ChangeBannerFromCategoryToBrand < ActiveRecord::Migration[5.1]
  def change
  	add_attachment :brands, :banner_image_url 
  end
end
