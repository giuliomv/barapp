class AddReferencesToUser < ActiveRecord::Migration[5.1]
  def change
  	add_reference :messages, :user, index: true
  	add_reference :orders, :user, index: true
  end
end
