class CreateCoupons < ActiveRecord::Migration[5.1]
  def change
    create_table :coupons do |t|
    	t.string :code, null: false
    	t.string :description
		t.integer :redemptions_limit, null: false
		t.integer :discount_amount, null: false
		t.date :valid_from, null: false
		t.date :valid_until, null: true
		t.integer :redemptions_count, null: false
		t.timestamps
    end
  end
end
