class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :contact_name
      t.string :delivery_address
      t.string :address_reference
      t.string :contact_phone
      t.decimal :total
      t.string :payment_method
      t.references :state, index: true

      t.timestamps
    end
  end
end
