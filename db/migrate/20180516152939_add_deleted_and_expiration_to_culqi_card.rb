class AddDeletedAndExpirationToCulqiCard < ActiveRecord::Migration[5.1]
  def change
  	add_column :culqi_cards, :deleted, :boolean, default: false
  end
end
