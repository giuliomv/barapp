class AddExtraFieldsForCombosCategories < ActiveRecord::Migration[5.1]
  def change
  	add_attachment :combos, :img_shopping_cart_url
  	add_column :combos, :order_key, :integer
  	add_column :categories, :order_key, :integer
  end
end
