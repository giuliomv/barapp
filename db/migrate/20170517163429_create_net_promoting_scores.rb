class CreateNetPromotingScores < ActiveRecord::Migration[5.1]
  def change
    create_table :net_promoting_scores do |t|
    	t.references :order, index: true
    	t.integer :numeric_score
    	t.text :comment
    	t.timestamps
    end
  end
end
