class AddOrderDeliveryFeeToOrder < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :order_delivery_fee, :decimal, :precision => 16, :scale => 10
  end
end
