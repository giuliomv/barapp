class AddDeletedToCombos < ActiveRecord::Migration[5.1]
  def change
  	add_column :combos, :deleted, :boolean
  	add_column :combos, :flg_stock, :boolean
  end
end
