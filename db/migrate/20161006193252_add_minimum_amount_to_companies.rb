class AddMinimumAmountToCompanies < ActiveRecord::Migration[5.1]
  def change
  	add_column :companies, :minimum_amount, :decimal, :precision => 16, :scale => 2
  end
end
