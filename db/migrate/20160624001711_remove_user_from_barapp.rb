class RemoveUserFromBarapp < ActiveRecord::Migration[5.1]
  def change
  	remove_column :messages, :user_id
  	remove_column :orders, :user_id
  	drop_table :users
  end
end
