class CreateActiveSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :active_schedules do |t|
      t.integer :day_of_week
      t.integer :initial_hour
      t.integer :initial_minutes
      t.integer :end_hour
      t.integer :end_minutes

      t.timestamps
    end
  end
end
