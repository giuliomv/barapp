class AddAuthTokenToAdminUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :admin_users, :authentication_token, :string
    add_index :admin_users, :authentication_token, unique: true
  end
end
