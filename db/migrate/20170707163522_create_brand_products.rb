class CreateBrandProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :brand_products do |t|
    	t.references :brand, index: true
    	t.string :name
    	t.attachment :img_v2_url
    	t.string :description
    	t.timestamps
    end
  end
end
