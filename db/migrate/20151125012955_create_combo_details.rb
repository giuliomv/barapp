class CreateComboDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :combo_details do |t|
      t.references :combo, index: true
      t.references :product, index: true
      t.integer :qty
      t.decimal :subtotal

      t.timestamps
    end
  end
end
