class AddLatAndLongToOrder < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :latitude, :decimal
  	add_column :orders, :longitude, :decimal
  end
end
