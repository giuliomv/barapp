class CreateDeliveryTiers < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_tiers do |t|
    	t.decimal :distance_limit, :precision => 16, :scale => 10
    	t.decimal :delivery_value, :precision => 16, :scale => 10
    	t.references :company, index: true
    	t.timestamps null: false
    end
  end
end
