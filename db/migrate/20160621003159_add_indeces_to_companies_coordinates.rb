class AddIndecesToCompaniesCoordinates < ActiveRecord::Migration[5.1]
  def change
  	add_index  :companies, [:latitude, :longitude]
  end
end
