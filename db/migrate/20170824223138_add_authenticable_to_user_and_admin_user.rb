class AddAuthenticableToUserAndAdminUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :authentications, :authenticable_type, :string
  	rename_column :authentications, :user_id, :authenticable_id
  end
end
