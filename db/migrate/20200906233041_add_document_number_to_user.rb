class AddDocumentNumberToUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :document_number, :string, default: ""
  	add_column :users, :accepts_ads, :boolean, default: false
  end
end
