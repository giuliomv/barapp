class ChangeRelationshipBetweenAdminUserAndCompany < ActiveRecord::Migration[5.1]
  def change
  	remove_reference :companies, :admin_user, index: true
  	add_reference :admin_users, :company, index: true
  end
end
