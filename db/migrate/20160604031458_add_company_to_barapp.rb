class AddCompanyToBarapp < ActiveRecord::Migration[5.1]
  def change
  	create_table :companies do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.string :ruc
      t.string :contact_id_doc
      t.string :contact_name
      t.decimal :latitude, :precision => 16, :scale => 10
      t.decimal :longitude, :precision => 16, :scale => 10
      t.timestamps
    end
    add_reference :products, :company, index: true
    add_reference :combos, :company, index: true
    add_reference :orders, :company, index: true
    add_reference :active_schedules, :company, index: true
    add_reference :schedule_texts, :company, index: true
    add_reference :companies, :admin_user, index: true
    
  end
end
