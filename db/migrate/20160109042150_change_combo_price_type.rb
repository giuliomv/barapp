class ChangeComboPriceType < ActiveRecord::Migration[5.1]
  def change
  	change_column :combos, :price, :decimal, :precision => 16, :scale => 2
  end
end
