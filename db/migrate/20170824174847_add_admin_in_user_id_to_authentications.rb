class AddAdminInUserIdToAuthentications < ActiveRecord::Migration[5.1]
  def change
  	add_reference :authentications, :admin_user, index: true
  end
end
