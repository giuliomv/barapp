class CreateComboOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :combo_orders do |t|
      t.references :combo, index: true
      t.references :order, index: true
      t.integer :qty
      t.decimal :subtotal

      t.timestamps
    end
  end
end
