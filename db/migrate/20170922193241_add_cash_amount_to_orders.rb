class AddCashAmountToOrders < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :cash_amount, :decimal, :precision => 16, :scale => 10
  end
end
