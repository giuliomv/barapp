class AppliedCouponToOrders < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :applied_coupon, :integer
  end
end
