class CreateBannerAds < ActiveRecord::Migration[5.1]
  def change
    create_table :banner_ads do |t|
		t.attachment :img_url
		t.string :name
    	t.timestamps null: false
    end
  end
end
