class ChangeProductPriceType < ActiveRecord::Migration[5.1]
  def change
  	change_column :products, :unitPrice, :decimal, :precision => 16, :scale => 2
  end
end
