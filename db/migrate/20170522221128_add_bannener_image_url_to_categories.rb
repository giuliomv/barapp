class AddBannenerImageUrlToCategories < ActiveRecord::Migration[5.1]
  def change
  	add_attachment :categories, :banner_image_url 
  end
end
