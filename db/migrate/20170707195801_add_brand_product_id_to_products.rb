class AddBrandProductIdToProducts < ActiveRecord::Migration[5.1]
  def change
  	add_reference :products, :brand_product, index: true
  end
end
