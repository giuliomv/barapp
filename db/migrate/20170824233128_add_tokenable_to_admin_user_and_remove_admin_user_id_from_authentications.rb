class AddTokenableToAdminUserAndRemoveAdminUserIdFromAuthentications < ActiveRecord::Migration[5.1]
  def change
  	remove_reference :authentications, :admin_user, index:true
  	add_column :device_tokens, :tokenable_type, :string
  	rename_column :device_tokens, :user_id, :tokenable_id
  end
end
