class AddDeliveryFeeToCompany < ActiveRecord::Migration[5.1]
  def change
  	add_column :companies, :delivery_fee, :decimal, :precision => 16, :scale => 10
  end
end
