class AddDecimalPrecisionToOrderDetailsComboDetailsComboOrders < ActiveRecord::Migration[5.1]
  def change
  	change_column :order_details, :subtotal, :decimal, :precision => 16, :scale => 2
  	change_column :combo_details, :subtotal, :decimal, :precision => 16, :scale => 2
  	change_column :combo_orders, :subtotal, :decimal, :precision => 16, :scale => 2
  end
end
