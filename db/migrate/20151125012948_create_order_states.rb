class CreateOrderStates < ActiveRecord::Migration[5.1]
  def change
    create_table :order_states do |t|
      t.string :name
      t.string :description
      t.boolean :deleted

      t.timestamps
    end
  end
end
