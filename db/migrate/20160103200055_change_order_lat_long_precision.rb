class ChangeOrderLatLongPrecision < ActiveRecord::Migration[5.1]
  def change
  	change_column :orders, :latitude, :decimal, :precision => 16, :scale => 10
  	change_column :orders, :longitude, :decimal, :precision => 16, :scale => 10
  end
end
