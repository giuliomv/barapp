class AddNewImageVersionsForProductsAndCombos < ActiveRecord::Migration[5.1]
  def change
  	add_attachment :products, :img_v2_url
  	add_attachment :combos, :img_v2_url
  end
end
