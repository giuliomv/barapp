class CreateCompanyBannerAds < ActiveRecord::Migration[5.1]
  def change
    create_table :company_banner_ads do |t|
		t.references :company, index: true
		t.references :banner_ad, index: true
		t.integer :order_key
    	t.timestamps null: false
    end
  end
end
