class AddPointsToOrderAndUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :fidelity_points, :integer
  	add_column :users, :total_fidelity_points, :integer, default: 0
  end
end
