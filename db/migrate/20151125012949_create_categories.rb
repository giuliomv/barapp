class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.boolean :deleted
      t.boolean :flg_avlb

      t.timestamps
    end
  end
end
