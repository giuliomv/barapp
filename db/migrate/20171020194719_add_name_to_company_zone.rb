class AddNameToCompanyZone < ActiveRecord::Migration[5.1]
  def change
  	add_column :company_zones, :name, :string
  end
end
