class RemoveColumnsFromProduct < ActiveRecord::Migration[5.1]
  def change
  	remove_column :products, :name
  	remove_column :products, :description
  	remove_reference :products, :brand, index:true
  	remove_attachment :products, :img_v2_url
  	remove_attachment :products, :img_url
  end
end
