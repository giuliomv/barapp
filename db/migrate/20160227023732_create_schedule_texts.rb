class CreateScheduleTexts < ActiveRecord::Migration[5.1]
  def change
    create_table :schedule_texts do |t|
      t.string :current_text
      t.timestamps
    end
  end
end
