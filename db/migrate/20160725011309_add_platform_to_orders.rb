class AddPlatformToOrders < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :platform, :string
  end
end
