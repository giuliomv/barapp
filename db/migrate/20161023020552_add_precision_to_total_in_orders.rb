class AddPrecisionToTotalInOrders < ActiveRecord::Migration[5.1]
  def change
  	change_column :orders, :total, :decimal, :precision => 16, :scale => 2
  end
end
