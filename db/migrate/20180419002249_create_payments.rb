class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
		t.integer :amount
		t.string :message_user
		t.string :message_merchant
		t.string :response_code
		t.string :state
		t.string :card_id
		t.string :id_charge
		t.references :order, index: true
      	t.timestamps null: false
    end
  end
end
